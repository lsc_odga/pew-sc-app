module.exports = {
  plugins: [
    require('autoprefixer') //note needed to downgrade to v9.8.6 (see https://github.com/parcel-bundler/parcel/issues/5160)
  ]
};