// import "core-js";
import 'bulma/css/bulma.css'
import "../css/main.scss";
import { bulmaEnableAllListeners } from '../js/utils/bulmaStuff.js'

// import drawMapboxMap from '../js/mapboxMap.js';
import { spaceEnterOrClick, mouseAndTabListener } from '../js/utils/accessibility.js'
import loadHomePage from "../js/pages/home.js";
import { dataConfig } from '../js/viz/dataConfig.js';

mouseAndTabListener();
bulmaEnableAllListeners();

window.addEventListener('DOMContentLoaded', (event) => {

	loadHomePage();

	/**
	 * We'll be using plenty of more modern JavaScript like promises and fetch and ES6, and we are not 
	 * intending to support older browsers like Internet Explorer. Here we use Modernizr to test for the
	 * presence of the fetch API in the browser. If it's not there then we display on the screen a warning
	 * to please use a more modern browser.
	 * https://caniuse.com/fetch
	 * https://caniuse.com/?search=promises
	 */
	/*! modernizr 3.6.0 (Custom Build) | MIT *
	* https://modernizr.com/download/?-fetch-setclasses !*/
	!function(n,e,s){function o(n,e){return typeof n===e}function a(){var n,e,s,a,t,l,r;for(var c in f)if(f.hasOwnProperty(c)){if(n=[],e=f[c],e.name&&(n.push(e.name.toLowerCase()),e.options&&e.options.aliases&&e.options.aliases.length))for(s=0;s<e.options.aliases.length;s++)n.push(e.options.aliases[s].toLowerCase());for(a=o(e.fn,"function")?e.fn():e.fn,t=0;t<n.length;t++)l=n[t],r=l.split("."),1===r.length?Modernizr[r[0]]=a:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=a),i.push((a?"":"no-")+r.join("-"))}}function t(n){var e=r.className,s=Modernizr._config.classPrefix||"";if(c&&(e=e.baseVal),Modernizr._config.enableJSClass){var o=new RegExp("(^|\\s)"+s+"no-js(\\s|$)");e=e.replace(o,"$1"+s+"js$2")}Modernizr._config.enableClasses&&(e+=" "+s+n.join(" "+s),c?r.className.baseVal=e:r.className=e)}var i=[],f=[],l={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(n,e){var s=this;setTimeout(function(){e(s[n])},0)},addTest:function(n,e,s){f.push({name:n,fn:e,options:s})},addAsyncTest:function(n){f.push({name:null,fn:n})}},Modernizr=function(){};Modernizr.prototype=l,Modernizr=new Modernizr,Modernizr.addTest("fetch","fetch"in n);var r=e.documentElement,c="svg"===r.nodeName.toLowerCase();a(),t(i),delete l.addTest,delete l.addAsyncTest;for(var u=0;u<Modernizr._q.length;u++)Modernizr._q[u]();n.Modernizr=Modernizr}(window,document);

	if (!Modernizr.fetch) {
		console.log('Non-modern-browser. Not all content will be loaded.');
		document.getElementById('non-modern-browser-notification').style.display = "block";
	}

	/**
	 * Add event listeners to each nav item to indicate which is the relevant script (page layout) to load. Pass in the 
	 * following: 1) the ID of the nav item, 2) the relevant script to build the layout or visualization, 3) an array [] of 
	 * arguments to use in the default function. See the function for intended args.
	 */

  function importOnEventListener(navID, scriptFile) {
    import(`../js/pages/${scriptFile}`)
    .then(function(module) {
      module.default(dataConfig[navID]);
    }).catch(function(error) {
      console.log(error);
    });
  }

	function addNavEventListeners(navID, scriptFile) {
		document.getElementById(navID).addEventListener('click', function(event) {
      importOnEventListener(navID, scriptFile)
    });
    document.getElementById(navID).addEventListener('keypress', function(event) {
      if (spaceEnterOrClick(event) === true) {
        importOnEventListener(navID, scriptFile)
      }
		});
	}

	addNavEventListeners('nav-item-home', 'home.js');
	addNavEventListeners('nav-item-representation-eviction', 'mapBars.js');
	addNavEventListeners('nav-item-representation-debt', 'mapBars.js');
	addNavEventListeners('nav-item-representation-foreclosure', 'mapBars.js');
	addNavEventListeners('nav-item-representation-circuit-c&d', 'mapBars.js');
	addNavEventListeners('nav-item-representation-magistrate-c&d', 'mapBars.js');
	addNavEventListeners('nav-item-need-poverty', 'mapBars.js');
	addNavEventListeners('nav-item-access-attorneys', 'mapBars.js');
	addNavEventListeners('nav-item-access-intakes', 'mapBars.js');
	addNavEventListeners('nav-item-profiles', 'profile.js');
	addNavEventListeners('nav-about', 'about.js');


  // Other event listeners for accessibility improvement
  //----------------------------------------------------

  // Nav menu dropdown list button shows or hides the dropdown list
  document.getElementById('nav-item-mapbar').addEventListener('keypress', function(event) {
    var code = event.charCode || event.code;
    if((code === 32)|| (code === 13)){
      event.preventDefault();
      if (document.getElementById('nav-mapbar-dropdown').style.display === 'block') {
        document.getElementById('nav-mapbar-dropdown').style.display = 'none';
        document.getElementById('nav-mapbar-dropdown').setAttribute('aria-expanded', 'false');
        document.getElementById('nav-item-mapbar').setAttribute('aria-hidden', 'true');
      } else {
        document.getElementById('nav-mapbar-dropdown').style.display = 'block'
        document.getElementById('nav-mapbar-dropdown').setAttribute('aria-expanded', 'true');
        document.getElementById('nav-item-mapbar').setAttribute('aria-hidden', 'false');
      }
    }
  })

  // Other nav items hide the dropdown list when pressed
  let navLinks = document.getElementsByClassName('navbar-item');
  for (var i=0; i<navLinks.length; i++) {
    if (navLinks[i].id !== 'navbar-dropdown') {
      navLinks[i].addEventListener('keypress', function(event) {
        var code = event.charCode || event.code;
        if((code === 32)|| (code === 13)){
          if (document.getElementById('nav-mapbar-dropdown').style.display === 'block') {
            document.getElementById('nav-mapbar-dropdown').style.display = 'none';
            document.getElementById('nav-mapbar-dropdown').setAttribute('aria-expanded', 'false');
            document.getElementById('nav-item-mapbar').setAttribute('aria-hidden', 'true');
          }
        }
      })
    }
  }

  // About popup show/hide listeners (trigger the JS functionality already assigned to click)
  document.getElementById('nav-about').addEventListener('keypress', function(event) {
    var code = event.charCode || event.code;
    if((code === 32)|| (code === 13)){
      event.preventDefault();
      document.getElementById('about-the-data').click();
    }
  });

  document.getElementById('close-about').addEventListener('keypress', function(event) {
    var code = event.charCode || event.code;
    if((code === 32)|| (code === 13)){
      event.preventDefault();
      document.getElementById('close-about').click();
    }
  });

});