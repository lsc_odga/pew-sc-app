const d3_scale = require('d3-scale');
const d3_array = require('d3-array');

export function calculateLinearColorScale(data, selectedVar, colorArray) {
	return d3_scale.scaleLinear()
		.domain([
			d3_array.min(data, function(d) { return d[selectedVar]; }),
			d3_array.max(data, function(d) { return d[selectedVar]; })
		])
		.range(colorArray)
		.unknown("#D3D3D3");
}