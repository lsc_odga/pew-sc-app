export function showPopup(popup, mouseX, mouseY, data, geoid) {

	popup.transition()
		.duration(150)
		.style("opacity", 0.9);

	if (mouseX < (window.innerWidth / 2)) {
		// Popup to the right
		popup
			.style("left", mouseX + 10 + "px")
			.style("top", (mouseY - 45) + "px");
	} else {
		// Popup to the left
		popup
			.style("left", mouseX - 165 + "px")
			.style("top", (mouseY - 45) + "px");
	}
	
}							

export function hidePopup(popup) {
	popup.transition()
		.duration(200)
		.style("opacity", 0);	
}

export function setPopupContents(popup, county, value, context) {
	if (!context || context === '') {
		popup.html(`<div class="popup-header">${county}</div><div class="popup-value">${value}</div>`)
	} else {
		popup.html(`<div class="popup-header">${county}</div><div class="popup-value">${value}</div><div class="popup-context">${context}</div>`)
	}
}