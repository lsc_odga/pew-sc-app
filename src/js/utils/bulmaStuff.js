// Global variable to track if the burger is open
let burgerOpen = false;

// Mutation observer to check when the burger class changes
// https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
export function watchBurger() {

	// Select the node that will be observed for mutations
	const burger = document.getElementById('navbar-burger');

	// Options for the observer (which mutations to observe)
	const config = { attributes: true };

	// Callback function to execute when mutations are observed
	const callback = function(mutationsList, observer) {
		// Use traditional 'for loops' for IE 11
		for(const mutation of mutationsList) {
			if (mutation.type === 'attributes') {
				burgerOpen = !burgerOpen;
			}
		}
	};

	// Create an observer instance linked to the callback function
	const observer = new MutationObserver(callback);

	// Start observing the target node for configured mutations
	observer.observe(burger, config);
}

export function openNavbarDropdown() {
	document.addEventListener('DOMContentLoaded', () => {
		
		// Shows all dropdown elements when they are mouseover
		document.getElementById('nav-item-mapbar').addEventListener('mouseover', () => {
			const $navbarDropdownElements = Array.prototype.slice.call(document.querySelectorAll('.navbar-dropdown'), 0);
			$navbarDropdownElements.forEach(el => {
				el.style.display = 'block';
			});
		});

		document.getElementById('navbar-burger').addEventListener('click', () => {
			console.log('show all burger elemetns ');
			const $navbarDropdownElements = Array.prototype.slice.call(document.querySelectorAll('.navbar-dropdown'), 0);
		 	$navbarDropdownElements.forEach(el => {
		 		el.style.display = 'block';
		 	});
		});

	});
}

export function closeNavbarDropdown() {

	document.addEventListener('DOMContentLoaded', () => {

		// Get all clickable navbar dropdown elements
		const $navbarDropdownClickable = Array.prototype.slice.call(document.querySelectorAll('.is-clickable'), 0);
		
		// Hide the dropdown elements on click
		$navbarDropdownClickable.forEach(el => {
			el.addEventListener('click', () => {
				const $navbarDropdownElements = Array.prototype.slice.call(document.querySelectorAll('.navbar-dropdown'), 0);
				$navbarDropdownElements.forEach(el => {
					el.style.display = 'none';
				});
				// Hide the burger if it is open
				if (burgerOpen) {
					// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
					const target = document.getElementById('navbar-burger').dataset.target;
					const $target = document.getElementById(target);
					document.getElementById('navbar-burger').classList.toggle('is-active');
					$target.classList.toggle('is-active');
				}
			});
		});

		// Hides dropdown elements when the mouse leaves the dropdown
		document.getElementById('navbar-dropdown').addEventListener('mouseleave', () => {
			const $navbarDropdownElements = Array.prototype.slice.call(document.querySelectorAll('.navbar-dropdown'), 0);
			$navbarDropdownElements.forEach(el => {
				el.style.display = 'none';
			});
		});

		// Watch the status of the burger
		watchBurger();

	});
}

export function bulmaNavBurgerListener() {
	document.addEventListener('DOMContentLoaded', () => {

		// Get all "navbar-burger" elements
		const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {

			// Add a click event on each of them
			$navbarBurgers.forEach(el => {

				el.addEventListener('click', () => {

					// Get the target from the "data-target" attribute
					const target = el.dataset.target;
					const $target = document.getElementById(target);

					// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
					el.classList.toggle('is-active');
					$target.classList.toggle('is-active');

				});
			});
		}

	});
}

export function bulmaEnableAllListeners() {
	bulmaNavBurgerListener();
	closeNavbarDropdown();
	openNavbarDropdown();
}