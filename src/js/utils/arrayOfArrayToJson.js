/**
 * The Census API returns data formatted as [[header1, header2],[data1, data2]].
 * Use this function to convert it to a more useable JSON.
 * From LazyDeveloper at https://stackoverflow.com/questions/44173385/array-of-arrays-to-json
 */
export function arrayOfArrayToJson(array) { 
	var objArray = [];
	for (var i = 1; i < array.length; i++) {
		objArray[i - 1] = {};
		for (var k = 0; k < array[0].length && k < array[i].length; k++) {
			var key = array[0][k];
			objArray[i - 1][key] = array[i][k]
		}
	}

	return objArray;
}