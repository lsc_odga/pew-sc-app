export function spaceEnterOrClick(event){
	if(event.type === 'click'){
		return true;
	}
	else if(event.type === 'keypress'){
			var code = event.charCode || event.code;
			if((code === 32)|| (code === 13)){
				event.preventDefault();
				return true;
			}
	}
	else{
			return false;
	}
}
//$(element).on('click keypress', function(event) {
//	if (spaceEnterOrClick(event) === true) {
//		do the stuff
//	}
//});

//------------------------------------------------------------------------------
export function mouseAndTabListener() {

	document.body.addEventListener('mousedown', function() {
		document.body.classList.add('using-mouse');
	});

	document.body.addEventListener('keydown', function(event) {
		document.body.classList.remove('using-mouse');
	});
}
//Add something this to the CSS...
// body.using-mouse :focus {
//   outline: none;
// }
