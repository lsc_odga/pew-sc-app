const d3_fetch = require('d3-fetch');
let firstVisit = true;

function showAbout(){
	document.getElementById('about-the-data').style.display = 'block';
  document.getElementById('nav-about').setAttribute('aria-expanded', 'true');
  document.getElementById('about-the-data').setAttribute('aria-hidden', 'false');
};

function hideAbout(){
	document.getElementById('about-the-data').style.display = 'none';
  document.getElementById('nav-about').setAttribute('aria-expanded', 'false');
  document.getElementById('about-the-data').setAttribute('aria-hidden', 'true');
};

export default function about() {
	// If it's the first visit, load the about data from the google sheet 
	if(firstVisit){
		// Update flag 
		firstVisit = false;

		// Load data
		const aboutCSV = d3_fetch.csv("https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=About");
		aboutCSV.then(aboutData => {
			let aboutText = "";
			for(let col of aboutData.columns){
				aboutText += `<div class="about-title">${col}</div>`
				for(let row of aboutData){
					if(row[col] != ""){
						aboutText += `<p class="about-text">${row[col]}</p>`
					};
				};
			}
			document.getElementById("about-text").innerHTML = aboutText;
		}).then(()=>{
			showAbout(); //show about modal with data
		});

		// Event listener to hide data
		document.getElementById("close-about").addEventListener("click", hideAbout);

	} else { //otherwise, just show the about modal
		showAbout();
	};
};