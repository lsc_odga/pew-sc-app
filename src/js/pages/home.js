// To-do: buttons to switch between need/access/representation sections on narrow screens

import { changeLayout } from './changeLayout.js';
import { getCensusData } from '../viz/censusData.js';
const d3_fetch = require('d3-fetch');
const d3_array = require('d3-array');
const d3_format = require('d3-format');
const Highcharts = require('highcharts');  
require('highcharts/modules/exporting')(Highcharts);  
require('highcharts/modules/data')(Highcharts);
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
NoDataToDisplay(Highcharts);
let year1, year2;

export default function loadHomePage() {

	// Underline only the current nav item in navbar
	document.getElementById("nav-item-mapbar").style.textDecoration = "none";
	document.getElementById("nav-item-home").style.textDecoration = "underline";
	document.getElementById("nav-item-profiles").style.textDecoration = "none";

	// Give statewide profile link text and redirect functionality
	document.getElementById("statewide-profile-note").innerHTML = `For more statewide data on South Carolina, <a id="link-to-statewide-profile" class="links">click here</a> to visit the state profile.`;
	document.getElementById("link-to-statewide-profile").addEventListener("click", function(){
		document.getElementById('nav-item-profiles').click();
	});

	changeLayout('page-layout-home');

	// Dropdown functionality on narrow screen
	document.getElementById("toggle-topic").addEventListener("change", function () {
		if (this.value == "need") {
			document.getElementById("c1wrapper").style.display = "flex";
			document.getElementById("c2wrapper").style.display = "none";
			document.getElementById("c3wrapper").style.display = "none";
		} else if (this.value == "access") {
			document.getElementById("c1wrapper").style.display = "none";
			document.getElementById("c2wrapper").style.display = "flex";
			document.getElementById("c3wrapper").style.display = "none";
		} else {
			document.getElementById("c1wrapper").style.display = "none";
			document.getElementById("c2wrapper").style.display = "none";
			document.getElementById("c3wrapper").style.display = "flex";
		};
	});

	// Read in section descriptions from google sheets
	const homeCaption = document.getElementById('home-intro');
	const needCaption = document.getElementById('need-intro');
	const accessCaption = document.getElementById('access-intro');
	const repCaption = document.getElementById('rep-intro');
	const captionsCSV = d3_fetch.csv("https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=Captions");

	captionsCSV.then(data => {
		homeCaption.innerHTML = data[0]['Home Caption'] + `<span id="link-to-report"> <a class="links">See the full report</a>.</span>`;
		needCaption.innerHTML = data[0]['Need Caption'];
		accessCaption.innerHTML = data[0]['Access Caption'];
		repCaption.innerHTML = data[0]['Rep Caption'];
	});


	// Set highcharts options for all charts
	Highcharts.setOptions({
		lang: {
			noData: "There is no data available for the selected year."
		},
		noData: {
			style: {
				color: 'darkred',
			}
		},
		chart: {
			style: {
				fontFamily: 'Segoe UI'
			},
			backgroundColor: 'transparent',
		},
		tooltip: {
			backgroundColor: 'rgba(255, 255, 255, 0.75)', //last parameter is opacity: https://stackoverflow.com/questions/24035938/highcharts-change-opacity-of-a-column-chart
			borderRadius: 15,
			borderWidth: 0,
			distance: 25,
			followPointer: true,
			style: {
				textAlign: 'center'
			}
		},
		credits: {
			enabled: false //removes highcharts.com logo
		},
		navigation: {
			buttonOptions: {
			enabled: false //removes burger menu
			}
		},
	});

	// Set up variables for census api call
	let nonSubjectTable = false;
	let subjectTable = true;
	let oneYearOrFive = '5';
	let forGeos = ['*'] //all counties
	let inGeos = ['45']; //in the state of south carolina

	let forGeoType = 'county';
	let inGeoType = 'state';

	const selectedYears = d3_fetch.csv("https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=Config");
	
	selectedYears.then(d => {
		// Take year from google sheet
		year1 = d[0]['Need Chart'];
		year2 = d[0]['Access Chart'];

		// population is a base table
		// let populationData = getCensusData(year1, oneYearOrFive, nonSubjectTable, ['NAME', 'B01003_001E'], forGeoType, forGeos, inGeoType, inGeos);
		let populationData = getCensusData(year1, oneYearOrFive, subjectTable, ['NAME', 'S1701_C01_001E'], forGeoType, forGeos, inGeoType, inGeos);
		// poverty is a subject table (need year)
		let povertyDataNeed = getCensusData(year1, oneYearOrFive, subjectTable, ['NAME', 'S1701_C02_001E', 'S1701_C01_039E'], forGeoType, forGeos, inGeoType, inGeos);
		// access
		let povertyDataAccess = getCensusData(year2, oneYearOrFive, subjectTable, ['NAME', 'S1701_C02_001E', 'S1701_C01_039E'], forGeoType, forGeos, inGeoType, inGeos);
		// import csv data
		const accessDataCSV = d3_fetch.csv("https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=Access");
		const repDataCSV = d3_fetch.csv("https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=Representation");
		const ruralityCSV = d3_fetch.csv("https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/166nXb2pUypnUna99n2kawbk6DTB3x4uCcy_G4RUqOaM/gviz/tq?tqx=out:csv&sheet=Rurality");
		return [populationData, povertyDataNeed, povertyDataAccess, accessDataCSV, repDataCSV, ruralityCSV];
	}).then(d => {
		Promise.all(d).then(datasets => {
			// Set up need object from population and poverty data
			const needMap = new Map();
			datasets[0].forEach(item => needMap.set(item['NAME'].split(" ")[0], item));
			datasets[1].forEach(item => needMap.set(item['NAME'].split(" ")[0], {...needMap.get(item['NAME'].split(" ")[0]), ...item}));
			const need = Array.from(needMap.values());
console.log('zzz', need)
			// Set up access object from accessDataCSV and poverty data
			const accessMap = new Map();
			datasets[2].forEach(item => accessMap.set(item['NAME'].split(" ")[0], item));
			datasets[3].forEach(item => accessMap.set(item['County'], {...accessMap.get(item['County']), ...item}));
			const access = Array.from(accessMap.values());

			const rep = datasets[4];

			// Rurality to key:value, then join to the needs dataset by county name
			let rurality = {};
			datasets[5].map(d => {
				rurality[d["County"]] = d["Rurality"];
			});
			need.forEach(d => {
				d['Rurality'] = rurality[d['NAME'].split(" ")[0]]
			})
			access.forEach(d => {
				d['Rurality'] = rurality[d.County]
			})

			// Build the highcharts format
			let needData = {}

			let pov125Total, pov100Total, popTotal, pov125Rural, pov100Rural, popRural, pov125Urban, pov100Urban, popUrban;

			pov125Total = d3_array.sum(need, d => d['S1701_C01_039E']);
			pov100Total = d3_array.sum(need, d => d['S1701_C02_001E']);
			popTotal = d3_array.sum(need, d => d['S1701_C01_001E']);
			pov125Rural = d3_array.sum(need.filter(d => d.Rurality === 'Rural'), d => d['S1701_C01_039E']);
			pov100Rural = d3_array.sum(need.filter(d => d.Rurality === 'Rural'), d => d['S1701_C02_001E']);
			popRural = d3_array.sum(need.filter(d => d.Rurality === 'Rural'), d => d['S1701_C01_001E']);
			pov125Urban = d3_array.sum(need.filter(d => d.Rurality === 'Urban'), d => d['S1701_C01_039E']);
			pov100Urban = d3_array.sum(need.filter(d => d.Rurality === 'Urban'), d => d['S1701_C02_001E']);
			popUrban = d3_array.sum(need.filter(d => d.Rurality === 'Urban'), d => d['S1701_C01_001E']);

			needData = [
				{  'pov': pov125Total,  'pop': popTotal, 'y': (pov125Total / popTotal) * 100 },
				{  'pov': pov125Rural,  'pop': popRural, 'y': (pov125Rural / popRural) * 100 },
				{  'pov': pov125Urban,  'pop': popUrban, 'y': (pov125Urban / popUrban) * 100 }
			]

			let accessDataIntakes = {}
			let accessDataAttorneys = {}

			let pov125TotalAccess, pov100TotalAccess, intakesTotal, attorneysTotal, pov125RuralAccess, pov100RuralAccess, intakesRural, attorneysRural, pov125UrbanAccess, pov100UrbanAccess, intakesUrban, attorneysUrban;

			pov125TotalAccess = d3_array.sum(access, d => d['S1701_C01_039E']);
			pov100TotalAccess = d3_array.sum(access, d => d['S1701_C02_001E']);
			intakesTotal = d3_array.sum(access, d => d['Intakes']);
			attorneysTotal = d3_array.sum(access, d => d['Attorneys']);
			pov125RuralAccess = d3_array.sum(access.filter(d => d.Rurality === 'Rural'), d => d['S1701_C01_039E']);
			pov100RuralAccess = d3_array.sum(access.filter(d => d.Rurality === 'Rural'), d => d['S1701_C02_001E']);
			intakesRural = d3_array.sum(access.filter(d => d.Rurality === 'Rural'), d => d['Intakes']);
			attorneysRural = d3_array.sum(access.filter(d => d.Rurality === 'Rural'), d => d['Attorneys']);
			pov125UrbanAccess = d3_array.sum(access.filter(d => d.Rurality === 'Urban'), d => d['S1701_C01_039E']);
			pov100UrbanAccess = d3_array.sum(access.filter(d => d.Rurality === 'Urban'), d => d['S1701_C02_001E']);
			intakesUrban = d3_array.sum(access.filter(d => d.Rurality === 'Urban'), d => d['Intakes']);
			attorneysUrban = d3_array.sum(access.filter(d => d.Rurality === 'Urban'), d => d['Attorneys']);

			accessDataIntakes = [
				{  'pov': pov125TotalAccess,  'var': intakesTotal, 'y': (intakesTotal / pov125TotalAccess) * 1000 },
				{  'pov': pov125RuralAccess,  'var': intakesRural, 'y': (intakesRural / pov125RuralAccess) * 1000 },
				{  'pov': pov125UrbanAccess,  'var': intakesUrban, 'y': (intakesUrban / pov125UrbanAccess) * 1000 }
			]

			accessDataAttorneys = [
				{  'pov': pov125TotalAccess,  'var': attorneysTotal, 'y': (attorneysTotal / pov125TotalAccess) * 1000 },
				{  'pov': pov125RuralAccess,  'var': attorneysRural, 'y': (attorneysRural / pov125RuralAccess) * 1000 },
				{  'pov': pov125UrbanAccess,  'var': attorneysUrban, 'y': (attorneysUrban / pov125UrbanAccess) * 1000 }
			]

			const repYears = [...new Set(rep.map( d => d.Year ) )];
			const repDataPlaint = {}
			const repDataDef = {}

			for (let i = 0; i < repYears.length; i++) {
				let debtVals = rep.filter(d => d.Year=== repYears[i] && d['Issue'] === 'Debt Collection')[0],
					foreclosureVals = rep.filter(d => d.Year=== repYears[i] && d['Issue'] === 'Foreclosure')[0],
					evicVals = rep.filter(d => d.Year=== repYears[i] && d['Issue'] === 'Eviction')[0],
					circVals = rep.filter(d => d.Year=== repYears[i] && d['Issue'] === 'Claim & Delivery (Circuit Court)')[0],
					magVals = rep.filter(d => d.Year=== repYears[i] && d['Issue'] === 'Claim & Delivery (Magistrate Court)')[0];
				
				let debtCases = debtVals["Total Cases"],
					debtPlaint = debtCases - debtVals["Both Represented"] - debtVals["Plaintiff Only Represented"],
					debtDef = debtCases - debtVals["Both Represented"] - debtVals["Defendant Only Represented"];

				let foreclosureCases = foreclosureVals["Total Cases"],
					foreclosurePlaint = foreclosureCases - foreclosureVals["Both Represented"] - foreclosureVals["Plaintiff Only Represented"],
					foreclosureDef = foreclosureCases - foreclosureVals["Both Represented"] - foreclosureVals["Defendant Only Represented"];

				let evicCases = evicVals["Total Cases"],
					evicPlaint = evicCases - evicVals['Both Represented'] - evicVals["Plaintiff Only Represented"],
					evicDef = evicCases - evicVals['Both Represented'] - evicVals["Defendant Only Represented"];
				
				let circCases = circVals["Total Cases"],
					circPlaint = circCases - circVals["Both Represented"] - circVals["Plaintiff Only Represented"],
					circDef = circCases - circVals["Both Represented"] - circVals["Defendant Only Represented"];

				let magCases = magVals["Total Cases"],
					magPlaint = magCases - magVals['Both Represented'] - magVals["Plaintiff Only Represented"],
					magDef = magCases - magVals['Both Represented'] - magVals["Defendant Only Represented"];

				repDataPlaint[`y${repYears[i]}`] = [
					{  'cases': debtCases,  'var': debtPlaint, 'y': (debtPlaint / debtCases) * 100 },
					{  'cases': foreclosureCases,  'var': foreclosurePlaint, 'y': (foreclosurePlaint / foreclosureCases) * 100 },
					{  'cases': circCases,  'var': circPlaint, 'y': (circPlaint / circCases) * 100 },
					{  'cases': magCases,  'var': magPlaint, 'y': (magPlaint / magCases) * 100 },
					{  'cases': evicCases,  'var': evicPlaint, 'y': (evicPlaint / evicCases) * 100 }
				]

				repDataDef[`y${repYears[i]}`] = [
					{  'cases': debtCases,  'var': debtDef, 'y': (debtDef / debtCases) * 100 },
					{  'cases': foreclosureCases,  'var': foreclosureDef, 'y': (foreclosureDef / foreclosureCases) * 100 },
					{  'cases': circCases,  'var': circDef, 'y': (circDef / circCases) * 100 },
					{  'cases': magCases,  'var': magDef, 'y': (magDef / magCases) * 100 },
					{  'cases': evicCases,  'var': evicDef, 'y': (evicDef / evicCases) * 100 }
				]	
			}

			// If there is only one year, hide the year dropdown
			// Else, populate select menu with year options from the set
			let yearDropdown = document.getElementById('year-select');
			
			if(repYears.size <=1){
				yearDropdown.style.display = "none";
			} else { 	
				yearDropdown.options.length = 0;
				for (let year of repYears) { 
					let option = document.createElement("option");
					option.value = `y${year}`;
					option.text = parseInt(year);
					yearDropdown.appendChild(option);
				};
				// Find the largest year in the set
				const maxYear = Math.max(...Array.from(repYears.values()));

				// Make that year the default option
				for (let i = 0; i < yearDropdown.options.length; i++) {
					let opt = yearDropdown.options[i];
					if (parseInt(opt.text) == maxYear){
						opt.selected = true;
					};
				};
			};

			// Global year variable and event listener
			let year = yearDropdown.value;
			yearDropdown.addEventListener('change', function(){
				year = yearDropdown.value;
				buildRepChart();
			});

			// Radio buttons for % poverty
			var accessRadio = document.forms["access-form"].elements["access-radio"];
			for(var i = 0, max = accessRadio.length; i < max; i++) {
				accessRadio[i].onclick = function() {
					if(this.value == "125p"){
						accessDataIntakes = [
							{  'pov': pov125TotalAccess,  'var': intakesTotal, 'y': (intakesTotal / pov125TotalAccess) * 1000 },
							{  'pov': pov125RuralAccess,  'var': intakesRural, 'y': (intakesRural / pov125RuralAccess) * 1000 },
							{  'pov': pov125UrbanAccess,  'var': intakesUrban, 'y': (intakesUrban / pov125UrbanAccess) * 1000 }
						]
						accessDataAttorneys = [
							{  'pov': pov125TotalAccess,  'var': attorneysTotal, 'y': (attorneysTotal / pov125TotalAccess) * 1000 },
							{  'pov': pov125RuralAccess,  'var': attorneysRural, 'y': (attorneysRural / pov125RuralAccess) * 1000 },
							{  'pov': pov125UrbanAccess,  'var': attorneysUrban, 'y': (attorneysUrban / pov125UrbanAccess) * 1000 }
						]
					};
					if(this.value == "100p"){
						accessDataIntakes = [
							{  'pov': pov100TotalAccess,  'var': intakesTotal, 'y': (intakesTotal / pov100TotalAccess) * 1000 },
							{  'pov': pov100RuralAccess,  'var': intakesRural, 'y': (intakesRural / pov100RuralAccess) * 1000 },
							{  'pov': pov100UrbanAccess,  'var': intakesUrban, 'y': (intakesUrban / pov100UrbanAccess) * 1000 }
						]
						accessDataAttorneys = [
							{  'pov': pov100TotalAccess,  'var': attorneysTotal, 'y': (attorneysTotal / pov100TotalAccess) * 1000 },
							{  'pov': pov100RuralAccess,  'var': attorneysRural, 'y': (attorneysRural / pov100RuralAccess) * 1000 },
							{  'pov': pov100UrbanAccess,  'var': attorneysUrban, 'y': (attorneysUrban / pov100UrbanAccess) * 1000 }
						]
					};
					buildAccessChart();
				};
			};

			var needRadio = document.forms["need-form"].elements["need-radio"];
			for(var i = 0, max = needRadio.length; i < max; i++) {
				needRadio[i].onclick = function() {
					if(this.value == "125p"){
						needData = [
							{  'pov': pov125Total,  'pop': popTotal, 'y': (pov125Total / popTotal) * 100 },
							{  'pov': pov125Rural,  'pop': popRural, 'y': (pov125Rural / popRural) * 100 },
							{  'pov': pov125Urban,  'pop': popUrban, 'y': (pov125Urban / popUrban) * 100 }
						]
					};
					if(this.value == "100p"){
						needData = [
							{  'pov': pov100Total,  'pop': popTotal, 'y': (pov100Total / popTotal) * 100 },
							{  'pov': pov100Rural,  'pop': popRural, 'y': (pov100Rural / popRural) * 100 },
							{  'pov': pov100Urban,  'pop': popUrban, 'y': (pov100Urban / popUrban) * 100 }
						]
					};
					buildNeedChart();
				};
			};

			// Function to build all charts
			function buildCharts(){
				buildNeedChart();
				buildAccessChart();
				buildRepChart();
			};

			// Functions to build individual charts
			function buildNeedChart(){

				let need_chart =  Highcharts.chart('need-container', {
					chart: {
						type: 'column',
					},
					tooltip : {
						outside: true,
						backgroundColor: '#FFFFFF',
						useHTML: true,
						formatter: function() {
							return `<div class="popup-header">${this.x.toUpperCase()}</div>
							<div class="popup-value">${this.y.toFixed(1)}%</div>
							<div class="popup-context">${d3_format.format(",")(this.series.userOptions.data[this.point.index]['pov'])} / ${d3_format.format(",")(this.series.userOptions.data[this.point.index]['pop'])} people</div>`
						}
					},
					legend: {
						enabled: false //no legend because there's only one variable
					},
					title: {
						text: 'South Carolina Poverty Rates'
					},
					subtitle: {
						useHTML: true,
        				text: `<a class="chart-subtitle" href="https://data.census.gov/cedsci/table?q=s1701&g=0400000US45&tid=ACSST5Y${year1}.S1701&hidePreview=true" target="_blank">Click here for chart data</a>`,
					},
					xAxis: {
						categories: ['Statewide', 'Rural', 'Urban'],
						labels: {
							useHTML: true,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}</p>`;
							}
						}
					},
					yAxis: {
						title: {
							enabled: true,
							text: '% of Population at 125% Poverty',
							style: {
								fontWeight: 'normal'
							}
						},
						labels: {
							useHTML: true,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}%</p>`;
							}
						}
					},
					series: [{
						color: '#384055',
						data: needData
					}]   
				});
			};

			function buildAccessChart(){
				let access_chart =  Highcharts.chart('access-container', {
					chart: {
						type: 'column'
					},
					tooltip : {
						outside: true,
						backgroundColor: '#FFFFFF',
						useHTML: true,
						formatter: function() {
							return `<p class="popup-header">${this.x.toUpperCase()}</p>
							<p class="popup-value">${this.y.toFixed(1)}</p>
							<p class="popup-context">${d3_format.format(",")(this.series.userOptions.data[this.point.index]['var'])} ${this.series.name.toLowerCase()} / ${d3_format.format(",")(this.series.userOptions.data[this.point.index]['pov'])} people in poverty</p>`
						}
					},
					title: {
						text: 'SCLS Intakes and Private Attorneys per 1,000 People in Poverty'
					},
					legend: {
						itemStyle: {
							color: '#000000',
							fontSize: '1rem',
							fontFamily: "Segoe UI",
							fontWeight: 'normal'
						},
					},
					xAxis: {
						categories: ['Statewide', 'Rural', 'Urban'],
						labels: {
							useHTML: true,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}</p>`;
							}
						}
					},
					yAxis: {
						title: {
							enabled: true,
							text: 'per 1,000 People in Poverty',
							style: {
								fontWeight: 'normal'
							}
						},
						labels: {
							useHTML: true,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}</p>`;
							}
						}
					},
					series: [{
						color: '#98b1bf',
						name: "Intakes",
						data: accessDataIntakes
					}, 
					{
						color: '#384055',
						name: "Attorneys",
						data: accessDataAttorneys
					}]
				});
			};

			function buildRepChart(){
				let rep_chart =  Highcharts.chart('rep-container', {
					chart: {
						type: 'column',
						zoomType: 'y',
					},
					plotOptions: {
						series: {
							minPointLength: 2
						}
					},
					tooltip : {
						outside: true,
						backgroundColor: '#FFFFFF',
						useHTML: true,
						formatter: function() {
							return `<p class="popup-header">${this.x.toUpperCase()}</p>
							<p class="popup-value">${this.y.toFixed(1)}% unrepresented</p>
							<p class="popup-context">${d3_format.format(",")(this.series.userOptions.data[this.point.index]['var'])} / ${d3_format.format(",")(this.series.userOptions.data[this.point.index]['cases'])} cases</p>
							`
						}
					},
					title: {
						text: 'Percent Unrepresented by Case Type'
					},
					legend : {
						itemStyle: {
							color: '#000000',
							fontSize: '1rem',
							fontFamily: "Segoe UI",
							fontWeight: 'normal',
							textOverflow: null,
						},
					},
					xAxis : {
						categories: ["Debt Collection", "Foreclosure", "Claim & Delivery (Circuit Court)", "Claim & Delivery (Magistrate Court)", "Eviction"],
						labels: {
							useHTML: true,
							style: {
								width:'115px',
							},
							rotation: 45,
							x: -15,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}</p>`;
							}
						}
					},
					yAxis: {
						max: 100,
						title: {
							enabled: true,
							text: '% Unrepresented',
							style: {
								fontWeight: 'normal'
							},
						},
						labels: {
							useHTML: true,
							formatter: function () {
								return `<p class="home-chart-label">${this.value}%</p>`;
							}
						}
					},
					series: [ 
					{
						color: '#98b1bf',
						name: "Plaintiffs",
						data: repDataPlaint[year]
					}, 
					{
						color: '#384055',
						name: "Defendants",
						data: repDataDef[year]
					}]
				});
			};
			// Initially build all charts
			buildCharts();
		});	
	})
}