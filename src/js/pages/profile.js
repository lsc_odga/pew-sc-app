import { changeLayout } from './changeLayout.js';
import { getCensusData } from '../viz/censusData.js';
const d3_selection = require('d3-selection');
const d3_fetch = require('d3-fetch');
const d3_format = require('d3-format');
const d3_array = require('d3-array');
const Highcharts = require('highcharts');  
require('highcharts/modules/exporting')(Highcharts);  
require('highcharts/modules/data')(Highcharts);
require('highcharts/highcharts-more')(Highcharts); 
require('highcharts/modules/dumbbell')(Highcharts); 
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
NoDataToDisplay(Highcharts);

let year;
let repDefMedians = [];
let repPlaintMedians = [];
let attorneyMedian = null;
let intakeMedian = null;
const profileDataUrl = "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=county_profiles";
const selectDropdownD3 = d3_selection.select('#county-profile-select');
const selectDropdownJS = document.getElementById('county-profile-select');
const counties = ["Abbeville", "Aiken", "Allendale", "Anderson", "Bamberg", "Barnwell", "Beaufort", "Berkeley", "Calhoun", "Charleston", "Cherokee", "Chester", "Chesterfield", "Clarendon", "Colleton", "Darlington", "Dillon", "Dorchester", "Edgefield", "Fairfield", "Florence", "Georgetown", "Greenville", "Greenwood", "Hampton", "Horry", "Jasper", "Kershaw", "Lancaster", "Laurens", "Lee", "Lexington", "Marion", "Marlboro", "McCormick", "Newberry", "Oconee", "Orangeburg", "Pickens", "Richland", "Saluda", "Spartanburg", "Sumter", "Union", "Williamsburg", "York"];
let firstVisit = true;

/* Populate the dropdown based on the google sheet, will only run once */
async function populateDropdown(){
  // Make dropdown set with default option to be disabled
  const defaultSet = new Set();
  defaultSet.add("Statewide");
  let dropdownSet = new Set([...defaultSet, ...counties]);
  
  // Populate dropdown
  selectDropdownD3.selectAll("option")
      .data(dropdownSet)
      .enter()
        .append("option")
        .attr("value", function(d) { return d; })
        .text(function(d) { return d })
        .each(function(d) {
          if (d === "Statewide"){
            d3_selection.select(this).property("selected", true);
          }
        });
};

/* Calculate medians from google sheet, will only run once */
async function calculateMedians(){

  // Clear values 
  repDefMedians = [];
  repPlaintMedians = [];
  attorneyMedian = null;
  intakeMedian = null;

  // Get county data
  let profileData = d3_fetch.csv(profileDataUrl);  
  
  if(repDefMedians.length == 0 || repPlaintMedians.length == 0 || attorneyMedian === null || intakeMedian === null){
    // If the medians haven't been calculated already
    await profileData.then(countyData => {
      // Declare rep variables
      let repVars = ['per_eviction', 'per_debt', 'per_foreclosure', 'per_circuit', 'per_magistrate'];
      let calculateDefMedian = {};
      let calculatePlaintMedian = {};
  
      // Create empty arrays for each rep variable
      for(let variable of repVars){
          calculateDefMedian[variable] = [];
          calculatePlaintMedian[variable] = [];
      };
  
      // Declare access variables
      let per_accepted = [];
      let attorneys_per_capita = [];
  
      // Populate arrays from Google sheet
      for(let i in countyData){
        calculateDefMedian['per_eviction'].push(countyData[i]['rep_eviction_def_unrep']/countyData[i]['rep_eviction_num_cases']);
        calculatePlaintMedian['per_eviction'].push(countyData[i]['rep_eviction_plaint_unrep']/countyData[i]['rep_eviction_num_cases']);
  
        calculateDefMedian['per_debt'].push(countyData[i]['rep_debt_def_unrep']/countyData[i]['rep_debt_num_cases']);
        calculatePlaintMedian['per_debt'].push(countyData[i]['rep_debt_plaint_unrep']/countyData[i]['rep_debt_num_cases']);
  
        calculateDefMedian['per_foreclosure'].push(countyData[i]['rep_foreclosure_def_unrep']/countyData[i]['rep_foreclosure_num_cases']);
        calculatePlaintMedian['per_foreclosure'].push(countyData[i]['rep_foreclosure_plaint_unrep']/countyData[i]['rep_foreclosure_num_cases']);
  
        calculateDefMedian['per_circuit'].push(countyData[i]['rep_circ_def_unrep']/countyData[i]['rep_circ_num_cases']);
        calculatePlaintMedian['per_circuit'].push(countyData[i]['rep_circ_plaint_unrep']/countyData[i]['rep_circ_num_cases']);
  
        calculateDefMedian['per_magistrate'].push(countyData[i]['rep_mag_def_unrep']/countyData[i]['rep_mag_num_cases']);
        calculatePlaintMedian['per_magistrate'].push(countyData[i]['rep_mag_plaint_unrep']/countyData[i]['rep_mag_num_cases']);
      
        per_accepted.push(countyData[i]['access_accepted_intakes']/countyData[i]['access_intakes'])
        attorneys_per_capita.push(countyData[i]['access_attorneys']/(countyData[i]['p125']/1000))
      };
  
      // Push rep medians to arrays
      for(let variable of repVars){
          repDefMedians.push(d3_array.median(calculateDefMedian[variable]));
          repPlaintMedians.push(d3_array.median(calculatePlaintMedian[variable]))
      };
  
      // Calculate access medians
      intakeMedian = d3_array.median(per_accepted);
      attorneyMedian = d3_array.median(attorneys_per_capita);
    });
  };
}

/* Build out representation section with county data */
async function makeRepSection(countyData){
  // Hide rep items relating to state only profile
  document.querySelectorAll('.rep-state-only').forEach(function(el) {
    el.style.display = 'none';
  });

  // Section names
  document.getElementById("rep-plaint-def").innerHTML = `Percent Unrepresented by Case Type`;
  // document.getElementById("rep-dumbbell").innerHTML = `${countyData[0]['county']} County v State Median`;
  document.getElementById("rep-dumbbell").innerHTML = `County v State Median`;

  // Button text
  document.getElementById("rep-radio-text").innerHTML = `Select a radio button below to show the % of cases with unrepresented defendants or the % of cases with unrepresented plaintiffs:`;

  // Initially defendants should be checked
  document.getElementById("rep-def").checked = true;
  document.getElementById("rep-legend-variable").innerHTML = `&nbsp ${countyData[0]['county']} Defendants`;
  document.getElementById("rep-legend-median").innerHTML = `&nbsp SC Median Defendants`;

  // Event listener for radio button to change between plaintiffs and defendants
  var repRadio = document.forms["profile-rep-form"].elements["rep"];
  for(var i = 0; i < repRadio.length; i++) {
    repRadio[i].onclick = async function() {
      makeRepCountyMedianChart(countyData, this.value);
    };
  };

  // After both charts are ready, hide the spinner
  Promise.all([makeRepPlaintiffDefendantChart(countyData), makeRepCountyMedianChart(countyData, 'rep-def')]).then( () => {
    contentReady("rep-loader", ".profile-rep");
    // Show rep items relating to county only profiles
    document.querySelectorAll('.rep-county-only').forEach(function(el) {
      el.style.visibility = 'visible';
    });
  });
};

/* Make the chart that shows county plaintiff or defendant representation vs SC median */
function makeRepCountyMedianChart(countyData, topic){
    // Get the county data for plaintiffs or defendants
    let county, median;

    if(topic == 'rep-def'){
      document.getElementById("rep-legend-variable").innerHTML = `&nbsp ${countyData[0]['county']} Defendants`;
      document.getElementById("rep-legend-median").innerHTML = `&nbsp SC Median Defendants`;
        county = [
            {'cases': parseFloat(countyData[0]['rep_eviction_def_unrep']), 'total': parseFloat(countyData[0]['rep_eviction_num_cases']), 'y': parseFloat(countyData[0]['rep_eviction_def_unrep'])/(parseFloat(countyData[0]['rep_eviction_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_debt_def_unrep']), 'total': parseFloat(countyData[0]['rep_debt_num_cases']), 'y': parseFloat(countyData[0]['rep_debt_def_unrep'])/(parseFloat(countyData[0]['rep_debt_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_foreclosure_def_unrep']), 'total': parseFloat(countyData[0]['rep_foreclosure_num_cases']), 'y': parseFloat(countyData[0]['rep_foreclosure_def_unrep'])/(parseFloat(countyData[0]['rep_foreclosure_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_circ_def_unrep']), 'total': parseFloat(countyData[0]['rep_circ_num_cases']), 'y': parseFloat(countyData[0]['rep_circ_def_unrep'])/(parseFloat(countyData[0]['rep_circ_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_mag_def_unrep']), 'total': parseFloat(countyData[0]['rep_mag_num_cases']), 'y': parseFloat(countyData[0]['rep_mag_def_unrep'])/(parseFloat(countyData[0]['rep_mag_num_cases']))}
        ];
        median = repDefMedians;
    } else{
      document.getElementById("rep-legend-variable").innerHTML = `&nbsp ${countyData[0]['county']} Plaintiffs`;
      document.getElementById("rep-legend-median").innerHTML = `&nbsp SC Median Plaintiffs`;
        county = [
            {'cases': parseFloat(countyData[0]['rep_eviction_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_eviction_num_cases']), 'y': parseFloat(countyData[0]['rep_eviction_plaint_unrep'])/(parseFloat(countyData[0]['rep_eviction_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_debt_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_debt_num_cases']), 'y': parseFloat(countyData[0]['rep_debt_plaint_unrep'])/(parseFloat(countyData[0]['rep_debt_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_foreclosure_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_foreclosure_num_cases']), 'y': parseFloat(countyData[0]['rep_foreclosure_plaint_unrep'])/(parseFloat(countyData[0]['rep_foreclosure_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_circ_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_circ_num_cases']), 'y': parseFloat(countyData[0]['rep_circ_plaint_unrep'])/(parseFloat(countyData[0]['rep_circ_num_cases']))},
            {'cases': parseFloat(countyData[0]['rep_mag_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_mag_num_cases']), 'y': parseFloat(countyData[0]['rep_mag_plaint_unrep'])/(parseFloat(countyData[0]['rep_mag_num_cases']))}
        ];
        median = repPlaintMedians;
    };

    let dumbbellData = [{
      name: 'Debt Collection',
      low: county[1]['y'],
      high: median[1]
    },
    {
      name: 'Foreclosure',
      low: county[2]['y'],
      high: median[2]
    },
    {
      name: 'Claim & Delivery (Circuit Court)',
      low: county[3]['y'],
      high: median[3]
    },
    {
      name: 'Claim & Delivery (Magistrate Court)',
      low: county[4]['y'],
      high: median[4]
    },
    {
      name: 'Eviction',
      low: county[0]['y'],
      high: median[0]
    }];

    Highcharts.chart('profile-rep-chart', {
      chart: {
        type: 'dumbbell',
        inverted: true,
        zoomType: 'x',
        height: '400px'
      },
      title: {
        text: undefined,
      },
      legend: {
        enabled: false,
      },
      xAxis : {
        categories: ["Debt Collection", "Foreclosure", "Claim & Delivery (Circuit Court)", "Claim & Delivery (Magistrate Court)", "Eviction"],
        labels: {
            useHTML: true,
            style: {
                width:'100px',
            },
            formatter: function () {
                return `<p class="profile-chart-category-label">${this.value}</p>`;
            }
        }
      },
      yAxis: {
          max: 1,
          title: {
            useHTML: true,
            text: '% Unrepresented',
            style: {
              fontSize: '1rem',
              fontFamily: "Segoe UI",
            },
          },
          labels: {
              formatter: function () {
                  return `<p class="profile-chart-category-label">${this.value*100}%</p>`;
              }
          }
      },
      tooltip : {
        useHTML: true,
        outside: true,
        backgroundColor: '#FFFFFF',
        formatter: function() {
          return `<p class="popup-header">${this.x}</p>
                  <p class="popup-value">${countyData[0]['county']} County: ${d3_format.format(".1%")(this.point.low)} unrepresented</p>
                  <p class="popup-value">SC Median: ${d3_format.format(".1%")(this.point.high)} unrepresented</p>`;
        }
      },
      series: [{
        data: dumbbellData,
        lowColor: "rgba(56, 64, 85, 0.7)",
        marker: {
          fillColor: "rgba(152, 177, 191, 0.7)", 
          radius: 6,
        }
      }]
    });
};

/* Build out the chart that shows plaintiff vs defendant representation in the county */
function makeRepPlaintiffDefendantChart(countyData){

  Highcharts.chart('profile-rep-comp-chart', {
    chart: {
      type: 'bar',
      zoomType: 'y',
      height: '400px'
    },
    plotOptions: {
      series: {
          minPointLength: 2
      }
    },
    title: {
      text: undefined,
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize: '.9rem'
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
        return `<p class="popup-header">${this.series.name}</p>
                <p class="popup-value">${this.series.userOptions.data[this.point.index]['type']}</p>
                <p class="popup-context">${d3_format.format(".1%")(this.y)} unrepresented</p>`;
      }
    },
    xAxis : {
      categories: ["Debt Collection", "Foreclosure", "Claim & Delivery (Circuit Court)", "Claim & Delivery (Magistrate Court)", "Eviction"],
      labels: {
          useHTML: true,
          style: {
              width:'100px',
          },
          formatter: function () {
              return `<p class="profile-chart-category-label">${this.value}</p>`;
          }
      }
    },
    yAxis: {
      max: 1,
      title: {
        useHTML: true,
        text: '% Unrepresented',
        style: {
          fontSize: '1rem',
          fontFamily: "Segoe UI",
        },
      },
      labels: {
          formatter: function () {
              return `<p class="profile-chart-category-label">${this.value*100}%</p>`;
          }
      }
    },
    series: [{
      color: '#98b1bf',
      name: "Plaintiffs",
      data:  [
        {'type': 'Debt Collection', 'cases': parseFloat(countyData[0]['rep_debt_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_debt_num_cases']), 'y': parseFloat(countyData[0]['rep_debt_plaint_unrep'])/(parseFloat(countyData[0]['rep_debt_num_cases']))},
        {'type': 'Foreclosure', 'cases': parseFloat(countyData[0]['rep_foreclosure_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_foreclosure_num_cases']), 'y': parseFloat(countyData[0]['rep_foreclosure_plaint_unrep'])/(parseFloat(countyData[0]['rep_foreclosure_num_cases']))},
        {'type': 'Circuit Claim & Delivery', 'cases': parseFloat(countyData[0]['rep_circ_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_circ_num_cases']), 'y': parseFloat(countyData[0]['rep_circ_plaint_unrep'])/(parseFloat(countyData[0]['rep_circ_num_cases']))},
        {'type': 'Magistrate Claim & Delivery', 'cases': parseFloat(countyData[0]['rep_mag_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_mag_num_cases']), 'y': parseFloat(countyData[0]['rep_mag_plaint_unrep'])/(parseFloat(countyData[0]['rep_mag_num_cases']))},
        {'type': 'Eviction', 'cases': parseFloat(countyData[0]['rep_eviction_plaint_unrep']), 'total': parseFloat(countyData[0]['rep_eviction_num_cases']), 'y': parseFloat(countyData[0]['rep_eviction_plaint_unrep'])/(parseFloat(countyData[0]['rep_eviction_num_cases']))}

      ]
    },{
      color: '#384055',
      name: `Defendants`,
      data: [
        {'type': 'Debt Collection', 'cases': parseFloat(countyData[0]['rep_debt_def_unrep']), 'total': parseFloat(countyData[0]['rep_debt_num_cases']), 'y': parseFloat(countyData[0]['rep_debt_def_unrep'])/(parseFloat(countyData[0]['rep_debt_num_cases']))},
        {'type': 'Foreclosure', 'cases': parseFloat(countyData[0]['rep_foreclosure_def_unrep']), 'total': parseFloat(countyData[0]['rep_foreclosure_num_cases']), 'y': parseFloat(countyData[0]['rep_foreclosure_def_unrep'])/(parseFloat(countyData[0]['rep_foreclosure_num_cases']))},
        {'type': 'Circuit Claim & Delivery', 'cases': parseFloat(countyData[0]['rep_circ_def_unrep']), 'total': parseFloat(countyData[0]['rep_circ_num_cases']), 'y': parseFloat(countyData[0]['rep_circ_def_unrep'])/(parseFloat(countyData[0]['rep_circ_num_cases']))},
        {'type': 'Magistrate Claim & Delivery', 'cases': parseFloat(countyData[0]['rep_mag_def_unrep']), 'total': parseFloat(countyData[0]['rep_mag_num_cases']), 'y': parseFloat(countyData[0]['rep_mag_def_unrep'])/(parseFloat(countyData[0]['rep_mag_num_cases']))},
        {'type': 'Eviction', 'cases': parseFloat(countyData[0]['rep_eviction_def_unrep']), 'total': parseFloat(countyData[0]['rep_eviction_num_cases']), 'y': parseFloat(countyData[0]['rep_eviction_def_unrep'])/(parseFloat(countyData[0]['rep_eviction_num_cases']))}

    ]}]  
  }); 
};

/* Functions to load data for the poverty chart */

// Variables to make Census API queries
let nonSubjectTable = false;
let subjectTable = true;
let oneYearOrFive = '5';
let forGeos;
let inGeos = ['45']; //south carolina
let forGeoType = 'county';
let inGeoType = 'state';

// Poverty rate data function
async function getPovData(type){
  let povQuery;
  if(type == "statewide"){
    povQuery = await getCensusData(year, oneYearOrFive, subjectTable, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E', 'S1701_C01_039E', 'S1701_C01_040E', 'S1701_C01_042E'], 'state', ['45']); 
  } else{
    povQuery = await getCensusData(year, oneYearOrFive, subjectTable, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E', 'S1701_C01_039E', 'S1701_C01_040E', 'S1701_C01_042E'], forGeoType, forGeos, inGeoType, inGeos);
    povQuery = povQuery.filter(x => x.NAME.replace(/ .*/,'') === selectDropdownJS.value) // Match county selection to first word in NAME field returned from the Census API
  }
  let pop = povQuery[0]['S1701_C01_001E'];
  let pov100 = povQuery[0]['S1701_C02_001E'];
  let pov125 = povQuery[0]['S1701_C01_039E'];
  let pov150 = povQuery[0]['S1701_C01_040E'];
  let pov200 = povQuery[0]['S1701_C01_042E'];
  let povData = [
      {'pov': pov100, 'total': pop, 'y': pov100/pop},
      {'pov': pov125, 'total': pop, 'y': pov125/pop},
      {'pov': pov150, 'total': pop, 'y': pov150/pop},
      {'pov': pov200, 'total': pop, 'y': pov200/pop},
  ]
  return [{ color: '#384055', data: povData }];
};

// Poverty age data function
async function getAgeData(type){
    let ageColumns = ['NAME', 'S1701_C01_002E', 'S1701_C01_007E', 'S1701_C01_008E', 'S1701_C01_010E',
    'S1701_C02_002E', 'S1701_C02_007E', 'S1701_C02_008E', 'S1701_C02_010E',];
    let ageQuery;
    if(type == "statewide"){
      ageQuery = await getCensusData(year, oneYearOrFive, subjectTable, ageColumns, 'state', ['45']); 
    } else{
      ageQuery = await getCensusData(year, oneYearOrFive, subjectTable, ageColumns, forGeoType, forGeos, inGeoType, inGeos);
      ageQuery = ageQuery.filter(x => x.NAME.replace(/ .*/,'') === selectDropdownJS.value)
    }
  
    let totalUnder18 = ageQuery[0]['S1701_C01_002E'];
    let povUnder18 =  ageQuery[0]['S1701_C02_002E'];
    let total18to34 = ageQuery[0]['S1701_C01_007E'];
    let pov18to34 = ageQuery[0]['S1701_C02_007E'];
    let total35to64 = ageQuery[0]['S1701_C01_008E'];
    let pov35to64 = ageQuery[0]['S1701_C02_008E'];
    let totalOver65 = ageQuery[0]['S1701_C01_010E'];
    let povOver65 = ageQuery[0]['S1701_C02_010E'];
    let ageData = [
        {'pov': povUnder18, 'total': totalUnder18, 'y': povUnder18/totalUnder18},
        {'pov': pov18to34, 'total': total18to34, 'y': pov18to34/total18to34},
        {'pov': pov35to64, 'total': total35to64, 'y': pov35to64/total35to64},
        {'pov': povOver65, 'total': totalOver65, 'y': povOver65/totalOver65}
    ]
    return [{ color: '#384055', data: ageData }];
};

// Poverty race data function
async function getRaceData(type){

    let raceColumns = ['NAME', 'S1701_C01_013E', 'S1701_C01_014E', 'S1701_C01_015E', 'S1701_C01_016E', 'S1701_C01_017E', 'S1701_C01_018E', 'S1701_C01_019E', 'S1701_C01_020E',
    'S1701_C02_013E', 'S1701_C02_014E', 'S1701_C02_015E', 'S1701_C02_016E', 'S1701_C02_017E', 'S1701_C02_018E', 'S1701_C02_019E', 'S1701_C02_020E'];

    let raceQuery;
    if(type == "statewide"){
      raceQuery = await getCensusData(year, oneYearOrFive, subjectTable, raceColumns, 'state', ['45']); 
    } else{
      raceQuery = await getCensusData(year, oneYearOrFive, subjectTable, raceColumns, forGeoType, forGeos, inGeoType, inGeos);
      raceQuery = raceQuery.filter(x => x.NAME.replace(/ .*/,'') === selectDropdownJS.value)
    };

    let povWhite = raceQuery[0]['S1701_C02_013E'];
    let totalWhite = raceQuery[0]['S1701_C01_013E'];

    let povBlack = raceQuery[0]['S1701_C02_014E'];
    let totalBlack = raceQuery[0]['S1701_C01_014E'];

    let povNative = raceQuery[0]['S1701_C02_015E'];
    let totalNative = raceQuery[0]['S1701_C01_015E'];

    let povAsian = raceQuery[0]['S1701_C02_016E'];
    let totalAsian = raceQuery[0]['S1701_C01_016E'];

    let povOther = raceQuery[0]['S1701_C02_017E'] + raceQuery[0]['S1701_C02_018E'];
    let totalOther = raceQuery[0]['S1701_C01_017E'] + raceQuery[0]['S1701_C01_018E'];

    let povBiracial = raceQuery[0]['S1701_C02_019E'];
    let totalBiracial = raceQuery[0]['S1701_C01_019E'];

    let povHispanic = raceQuery[0]['S1701_C02_020E'];
    let totalHispanic = raceQuery[0]['S1701_C01_020E'];

    let raceData = [
        {'pov': povWhite, 'total': totalWhite, 'y': povWhite/totalWhite},
        {'pov': povBlack, 'total': totalBlack, 'y': povBlack/totalBlack},
        {'pov': povNative, 'total': totalNative, 'y': povNative/totalNative},
        {'pov': povAsian, 'total': totalAsian, 'y': povAsian/totalAsian},
        {'pov': povOther, 'total': totalOther, 'y': povOther/totalOther},
        {'pov': povBiracial, 'total': totalBiracial, 'y': povBiracial/totalBiracial},
        {'pov': povHispanic, 'total': totalHispanic, 'y': povHispanic/totalHispanic}
    ]

    return [{ color: '#384055', data: raceData }];
};

// Poverty vet data function
async function getVetData(type){

    let vetColumns = ['NAME', 'C21007_003E', 'C21007_018E', 'C21007_004E', 'C21007_019E', 'C21007_010E', 'C21007_011E', 'C21007_025E', 'C21007_026E'];

    let vetQuery;
    if(type == "statewide"){
      vetQuery = await getCensusData(year, oneYearOrFive, nonSubjectTable, vetColumns, 'state', ['45']); 
    } else{
      vetQuery = await getCensusData(year, oneYearOrFive, nonSubjectTable, vetColumns, forGeoType, forGeos, inGeoType, inGeos);
      vetQuery = vetQuery.filter(x => x.NAME.replace(/ .*/,'') === selectDropdownJS.value)
    };

    let totalVets = vetQuery[0]['C21007_003E'] + vetQuery[0]['C21007_018E'];
    let povVets =  vetQuery[0]['C21007_004E'] + vetQuery[0]['C21007_019E'];

    let totalNonVet = vetQuery[0]['C21007_010E'] + vetQuery[0]['C21007_025E'];
    let povNonVet = vetQuery[0]['C21007_011E'] + vetQuery[0]['C21007_026E'];

    let vetData = [
        {'pov': povVets, 'total': totalVets, 'y': povVets/totalVets},
        {'pov': povNonVet, 'total': totalNonVet, 'y': povNonVet/totalNonVet},
    ]

    return [{ color: '#384055', data: vetData }];
};

// Poverty disability data function 
async function getDisabilityData(type){

    let disabilityColumns = ['NAME', 'B23024_003E', 'B23024_010E', 'B23024_018E', 'B23024_025E']; 

    let disabilityQuery;
    if(type == "statewide"){
      disabilityQuery = await getCensusData(year, oneYearOrFive, nonSubjectTable, disabilityColumns, 'state', ['45']); 
    } else{
      disabilityQuery = await getCensusData(year, oneYearOrFive, nonSubjectTable, disabilityColumns, forGeoType, forGeos, inGeoType, inGeos);
      disabilityQuery = disabilityQuery.filter(x => x.NAME.replace(/ .*/,'') === selectDropdownJS.value)
    };

    let povDisability = disabilityQuery[0]['B23024_003E'];
    let povNoDisability = disabilityQuery[0]['B23024_010E'];
    let noPovDisability = disabilityQuery[0]['B23024_018E'];
    let noPovNoDisability = disabilityQuery[0]['B23024_025E'];

    let disabilityData = [
      {'pov': povDisability, 'total': povDisability+noPovDisability, 'y': povDisability/(povDisability+noPovDisability)},
      {'pov': povNoDisability, 'total': povNoDisability+noPovNoDisability, 'y': povNoDisability/(povNoDisability+noPovNoDisability)}
    ]

    return [{ color: '#384055', data: disabilityData }];
};

// Define different details for the poverty chart based on the selected topic
let povChartDetails = {
    'pov-rate': {
      'title': 'Percent of People in Poverty by Poverty Level',
      'x-axis' : {
        'categories': ['100% poverty level', '125% poverty level', '150% poverty level', '200% poverty level'],
      },
      'y-axis': {
        'text': 'Percent at Poverty Level',
      }
    },
    'pov-age': {
      'title': 'People in Poverty by Age',
      'x-axis' : {
        'categories': ['Under 18', '18 to 34', '35 to 64', '65 and Over'],
      },
      'y-axis': {
        'text': 'Percent in Poverty',
      }
    },
    'pov-race': {
      'title': 'People in Poverty by Race/Ethinicity',
      'x-axis' : {
        'categories': ['White', 'African American', 'Native American', 'Asian', 'Other race', 'Two or more races', "Hispanic/Latino (any race)"],
      },
      'y-axis': {
        'text': 'Percent in Poverty',
      }
    },
    'pov-vet': {
      'title': 'People 18 and Over in Poverty by Veteran Status',
      'x-axis' : {
        'categories': ['Veteran', 'Non-Veteran'],
      },
      'y-axis': {
        'text': 'Percent in Poverty',
      }
    },
    'pov-disability': {
      'title': 'People 20 to 64 in Poverty by Disability',
      'x-axis' : {
        'categories': ['Disability', 'No disability'],
      },
      'y-axis': {
        'text': 'Percent in Poverty',
      }
    }
};

// Define the URLs of the chart's data based on the selected topic
function chartLinks(chart, year, type, forGeos){
  if(chart == 'pov-rate' || chart == 'pov-age' || chart == 'pov-race'){
    if(type == 'statewide'){
      return `https://data.census.gov/cedsci/table?q=S1701&g=0400000US45&tid=ACSST5Y${year}.S1701&hidePreview=true`;
    } else{
      return `https://data.census.gov/cedsci/table?q=S1701&g=0500000US45${forGeos[0]}&tid=ACSST5Y${year}.S1701&hidePreview=true`
    };
  } else if (chart == "pov-vet"){
    if(type == 'statewide'){
      return `https://data.census.gov/cedsci/table?q=C21007&g=0400000US45&tid=ACSDT5Y${year}.C21007&hidePreview=true`;
    } else{
      return `https://data.census.gov/cedsci/table?q=C21007&g=0500000US45${forGeos[0]}&tid=ACSDT5Y${year}.C21007&hidePreview=true`
    };
  } else if (chart == "pov-disability"){
    if(type == 'statewide'){
      return `https://data.census.gov/cedsci/table?q=B23024&g=0400000US45&tid=ACSDT5Y${year}.B23024&hidePreview=true`
    } else{
      return `https://data.census.gov/cedsci/table?q=B23024&g=0500000US45${forGeos[0]}&tid=ACSDT5Y${year}.B23024&hidePreview=true`
    };
  } else{
    return 'No link available';
  };
};

// Function to call the right poverty data function based on the radio button selection 
async function callPovFunction(topic, type){
  if(topic === 'pov-rate'){
    return await getPovData(type);
  };
  if(topic === 'pov-age'){
    return await getAgeData(type);
  };
  if(topic === 'pov-race'){
    return await getRaceData(type);
  };
  if(topic === 'pov-vet'){
    return await getVetData(type);
  };
  if(topic === 'pov-disability'){
    return await getDisabilityData(type);
  };
};

/* Build out the need section */
async function makeNeedSection(fips, county, type){
    // Reset radio button to default poverty chart
    document.getElementById("pov-rate").checked = true;

    // Use fips to set forGeos for census API calls
    let countyNo = fips.substr(fips.length - 3);
    let forGeos = [countyNo];

    // Radio buttons to load other data on changes
    var povRadio = document.forms["pov-form"].elements["pov"];
    for(var i = 0; i < povRadio.length; i++) {
      povRadio[i].onclick = async function() {
        // Call data function for radio button topic
        let chartData = await callPovFunction(this.value, type);
        makePovertyChart([chartData, this.value, type, county, forGeos]);
      };
    };
    
    // Initially load poverty level data and show that chart
    let povertyData = await getPovData(type);

    // Once the chart and table are done, hide the spinner
    Promise.all([makePovertyChart([povertyData, 'pov-rate', type, county, forGeos]), makeNeedTable(type, forGeos)]).then( ()=> {
      contentReady("need-loader", ".profile-need");
    });
};

// Build out cost burden table in the need section
async function makeNeedTable(type, forGeos){

  let renterLink, ownerLink;
  if(type === "statewide"){
    renterLink = `https://data.census.gov/cedsci/table?q=B25070&g=0400000US45&tid=ACSDT5Y${year}.B25070&hidePreview=true`;
    ownerLink = `https://data.census.gov/cedsci/table?q=B25091&g=0400000US45&tid=ACSDT5Y${year}.B25091&hidePreview=true`;
  } else{
    renterLink = `https://data.census.gov/cedsci/table?q=B25070&g=0500000US45${forGeos[0]}&tid=ACSDT5Y${year}.B25070&hidePreview=true`;
    ownerLink = `https://data.census.gov/cedsci/table?q=B25091&g=0500000US45${forGeos[0]}&tid=ACSDT5Y${year}.B25091&hidePreview=true`;
  };

  //Section subtitle
  document.getElementById("need-poverty").innerHTML = "Poverty";

  // Summary stat
  let countyPop, countyPov;
  let povStatsData;
  if(type == "statewide"){
    povStatsData = await getCensusData(year, oneYearOrFive, subjectTable, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E', 'S1701_C03_001E', 'S1602_C03_001E', 'S1701_C01_039E'], 'state', ['45']); 
  } else{
    povStatsData = await getCensusData(year, oneYearOrFive, subjectTable, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E', 'S1701_C03_001E', 'S1602_C03_001E', 'S1701_C01_039E'], forGeoType, forGeos, inGeoType, inGeos);
  };
  countyPop = d3_format.format(",")(povStatsData[0]['S1701_C01_001E']);
  countyPov = d3_format.format(",")(povStatsData[0]['S1701_C01_039E']);
  document.getElementById("pov-stats-container").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(".1%")(parseInt(countyPov.replaceAll(',', ''))/parseInt(countyPop.replaceAll(',', '')))}</span> of the population has income below 125% of the poverty line</p><div class="banner-sub-text">(${countyPov} people out of ${countyPop})</div>`;

  // Cost burden of housing
  document.getElementById("need-cost-burden").innerHTML = `Housing Cost Burden`;
  document.getElementById("need-table-title").innerHTML = `Percent of Households Spending Over 30% or 50% of Income on Housing`;
  document.getElementById("need-table-link").innerHTML = `<a class="chart-subtitle" href="#" onclick="window.open('${renterLink}'); window.open('${ownerLink}')">Click here for table data</a>`;

  let totalRenters, rentersCostBurden, rentersSevereCostBurden, totalOwners, ownerCostBurden, ownerSevereCostBurden;
  let costBurdenCols = ['NAME', 'B25070_001E', 'B25070_007E', 'B25070_008E', 'B25070_009E', 'B25070_010E', 'B25091_002E', 'B25091_008E', 'B25091_009E', 'B25091_010E', 'B25091_011E'];
  let costBurdenData;
  if(type == "statewide"){
    costBurdenData = await getCensusData(year, oneYearOrFive, nonSubjectTable, costBurdenCols, 'state', ['45']); 
  } else{
    costBurdenData = await getCensusData(year, oneYearOrFive, nonSubjectTable, costBurdenCols, forGeoType, forGeos, inGeoType, inGeos);
  };
  console.log(costBurdenData)
  totalRenters = costBurdenData[0]['B25070_001E'];
  rentersCostBurden = costBurdenData[0]['B25070_007E'] + costBurdenData[0]['B25070_008E'] + costBurdenData[0]['B25070_009E']; 
  rentersSevereCostBurden = costBurdenData[0]['B25070_010E'];

  totalOwners = costBurdenData[0]['B25091_002E'];
  ownerCostBurden = costBurdenData[0]['B25091_008E'] + costBurdenData[0]['B25091_009E'] + costBurdenData[0]['B25091_010E']; 
  ownerSevereCostBurden = costBurdenData[0]['B25091_011E'];

  document.getElementById("need-table").innerHTML = `<tr>
                                                        <td></td>
                                                        <td>Renter</td>
                                                        <td>Owner</td>
                                                      </tr>
                                                      <tr>
                                                        <td>30-50% of income</td>
                                                        <td>${d3_format.format(".1%")(rentersCostBurden/totalRenters)}</td>
                                                        <td>${d3_format.format(".1%")(ownerCostBurden/totalOwners)}</td>
                                                      </tr>
                                                      <tr>
                                                        <td>50% or more of income</td>
                                                        <td>${d3_format.format(".1%")(rentersSevereCostBurden/totalRenters)}</td>
                                                        <td>${d3_format.format(".1%")(ownerSevereCostBurden/totalOwners)}</td>
                                                      </tr>`;
};

// Build the poverty chart
function makePovertyChart(d){

  let povertyLink, tooltipName;
  if(d[2] === "statewide"){
    tooltipName = "South Carolina";
    povertyLink = chartLinks(d[1], year, 'statewide', d[4]);
  } else{
    tooltipName = d[3];
    povertyLink = chartLinks(d[1], year, 'county', d[4]);
  };

    // Obtain max y-value for chart
    let y = []
    if(d[0] === null){
      console.log("No data")
      y = [0]
    } else{
      d[0][0]['data'].forEach(element => y.push(element['y']));
    };

    Highcharts.chart('pov-chart', {
      lang: {
        noData: "There is no data for the selected county for this chart."
      },
      noData: {
        style: {
          color: 'darkred',
        }
      },
      chart: {
        type: 'bar',
      },
      legend: {
        enabled: false,
      },
      title: {
        text: povChartDetails[d[1]]['title'],
        useHTML: true,
        widthAdjust: 0,
        style: {
          color: '#384055',
          fontSize: '0.9rem',
          fontFamily: "Copse",
          padding: "0.125rem 1rem",
          fontWeight: 'bold'
        },
      },
      subtitle: {
        useHTML: true,
        text: `<a class="chart-subtitle" href="${povertyLink}" target="_blank">Click here for chart data</a>`,
      },
      xAxis: {
        title: {
          enabled: null,
        },
        categories: povChartDetails[d[1]]['x-axis']['categories'],
        labels: { 
            useHTML: true,
            formatter: function () { return `<p class="profile-chart-category-label">${this.value}</p>` }
        }
      },
      yAxis: { 
        max: d3_array.max(y)*1.05,
        title: {
          useHTML: true,
          text: povChartDetails[d[1]]['y-axis']['text'],
          style: {
            fontSize: '1rem',
            fontFamily: "Segoe UI",
          },
        },
        labels: {
          formatter: function () {
            return `<p class="profile-chart-category-label">${d3_format.format(".0%")(this.value)}</p>`;
          }
        }
      },
      tooltip : {
        useHTML: true,
        outside: true,
        backgroundColor: '#FFFFFF',
        formatter: function() {
          return `<p class="popup-header">${tooltipName.toUpperCase()}</p>
          <p class="popup-value">${d3_format.format(".1%")(this.y)}</p>
          <p class="popup-context">${d3_format.format(",")(this.point.pov)} of ${d3_format.format(",")(this.total)}</p>
          `
        }
      },
      plotOptions: {
        series: {
            minPointLength: 3 //so you can still hover and see the tooltip when the numerator is 0
        }
      },
      series: d[0]
    });
};

/* Build out the access section */
async function makeAccessSection(countyData){
  // Show subheading
  document.getElementById("access-attorneys").innerHTML = `Attorneys`;
  document.getElementById("access-intakes").innerHTML = `Intakes`;

  // Show attorney stats
  document.getElementById("access-stats-container").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(",")(countyData[0]["access_attorneys"])}</span> attorneys for <span class="banner-text">${d3_format.format(",")(countyData[0]["p125"])}</span> people below 125% of the poverty line</p>`;

  // Show intakes stats
  document.getElementById("access-intakes-stats").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(",")(countyData[0]["access_accepted_intakes"])}</span> intakes accepted of <span class="banner-text">${d3_format.format(",")(countyData[0]["access_intakes"])}</span> total intakes</p>`;

  // Once charts and tables are ready, hide the spinner
  Promise.all([makeAttorneyChart(countyData),  makeIntakeChart([countyData[0]['access_accepted_intakes'], countyData[0]['access_intakes'], countyData[0]['county']]), displayInternetAccess("county", countyData[0]['fips'])]).then( () => {
    contentReady("access-loader", ".profile-access");
  });

};

// Make the attorney line chart
function makeAttorneyChart(countyData){

  let countyVal = parseFloat(countyData[0]["access_attorneys"])/(parseFloat(countyData[0]['p125'])/1000);

  Highcharts.chart('access-attorney-chart', {
    chart: {
      type: "scatter",
      renderTo: "chart",
    },
    title: {
      align: 'center',
      text:  `Attorneys per 1,000 People Below 125% of the Poverty Line`,
      useHTML: true,
      style: {
        color: '#384055',
        fontSize: '.9rem',
        fontFamily: "Copse",
        padding: "0.125rem 1rem",
        fontWeight: 'bold'
      },
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize:'.9rem',
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
          if(this.series.name == "SC Median"){
              return `<p class="popup-header">SC Median</p>
                      <p class="popup-value">${d3_format.format(",.1f")(this.x)}`
          } else{
              return `<p class="popup-header">${countyData[0]['county']}</p>
              <p class="popup-value">${d3_format.format(",.1f")(this.x)}</p>
              <p class="popup-context">${d3_format.format(",")(countyData[0]["access_attorneys"])} attorneys for ${d3_format.format(",")(countyData[0]['p125'])} people below 125% poverty.</p>`
          }
      }
    },
    xAxis: {
      min: 0.95*d3_array.min([countyVal, attorneyMedian]),
      max: 1.05*d3_array.max([countyVal, attorneyMedian]),
      labels: {
        formatter: function () {
          return `<p class="profile-chart-category-label">${d3_format.format(".3")(this.value)}</p>`;
        }
      }
    },
    yAxis: {
      max: 0,
      min : 0,
      minRange :0.1,
      maxPadding: .01,
      lineWidth: 0,
      tickWidth: 2,
      tickLength: 6,
      labels: {
          y: 3
      },
      tickInterval: 1,
      labels: {
        enabled: !1
      },
      title : {
        enabled: false,
      }, 
      tickWidth: 0,
      tickLength: 0,
      maxPadding: 0,
    },
    series: [{
        name: `${countyData[0]['county']}`,
        color: "#384055",
        data: [[parseFloat(countyData[0]["access_attorneys"])/(parseFloat(countyData[0]['p125'])/1000), 0]]}, 
        {
        name: "SC Median",
        color: '#98b1bf',
        data: [[attorneyMedian, 0]],
        }]
  });
};

// Make the internet access table
async function displayInternetAccess(type, fips){

  let internetData, chartLink;
  let internetCols = ['NAME', 'B28004_005E', 'B28004_009E', 'B28004_013E', 'B28004_017E', 'B28004_021E', 'B28004_025E', 'B28004_002E', 'B28004_006E', 'B28004_010E', 'B28004_014E', 'B28004_018E', 'B28004_022E'];
  if(type === "statewide"){
    internetData = await getCensusData(year, oneYearOrFive, nonSubjectTable, internetCols, "state", ['45']);		
    chartLink = `https://data.census.gov/cedsci/table?q=B28004&g=0400000US45&tid=ACSDT5Y${year}.B28004&hidePreview=true`;
  } else{ //County
    let countyNo = fips.substr(fips.length - 3);
    let forGeos = [countyNo];
    chartLink = `https://data.census.gov/cedsci/table?q=B28004&g=0500000US45${countyNo}&tid=ACSDT5Y${year}.B28004&hidePreview=true`;
    internetData = await getCensusData(year, oneYearOrFive, nonSubjectTable, internetCols, forGeoType, forGeos, inGeoType, inGeos);		
  };
  
  let iL20, i20to35, i35to50, iG50, iL20Pop, i20to35Pop, i35to50Pop, iG50Pop, totalWithoutI, totalHouseholds;

  iL20 = internetData[0]['B28004_005E'] + internetData[0]['B28004_009E'];
  i20to35 = internetData[0]['B28004_013E'];
  i35to50 = internetData[0]['B28004_017E'];
  iG50 = internetData[0]['B28004_021E'] + internetData[0]['B28004_025E'];

  totalWithoutI = iL20 + i20to35 + i35to50 + iG50;

  iL20Pop = internetData[0]['B28004_002E'] + internetData[0]['B28004_006E'];
  i20to35Pop = internetData[0]['B28004_010E'];
  i35to50Pop = internetData[0]['B28004_014E'];
  iG50Pop = internetData[0]['B28004_018E'] + internetData[0]['B28004_022E'];

  totalHouseholds = iL20Pop + i20to35Pop + i35to50Pop + iG50Pop;

  let internet = [
    {'homes': iL20, 'total': iL20Pop, 'y': iL20/iL20Pop},
    {'homes': i20to35, 'total': i20to35Pop, 'y': i20to35/i20to35Pop},
    {'homes': i35to50, 'total': i35to50Pop, 'y': i35to50/i35to50Pop},
    {'homes': iG50, 'total': iG50Pop, 'y': iG50/iG50Pop}
  ];

  document.getElementById("access-internet").innerHTML = `Internet`;

  document.getElementById("access-internet-stat").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(".1%")(totalWithoutI/totalHouseholds)}</span> of households lack internet access</p><div class="banner-sub-text">(${d3_format.format(",")(totalWithoutI)} households out of ${d3_format.format(",")(totalHouseholds)})</div>`;
  document.getElementById("access-table-link").innerHTML = `<a class="chart-subtitle" href="${chartLink}" target="_blank">Click here for table data</a>`;

  // Show internet access table
  document.getElementById("access-internet-table-title").innerHTML = `Percent of Households Without Internet Access by Income`;
  document.getElementById("access-internet-table1").innerHTML = `<tr>
                                                        <td><$20K</td>
                                                        <td>$20K-$34.9K</td>
                                                      </tr>
                                                      <tr>
                                                        <td>${d3_format.format(".1%")(internet[0]['y'])}</td>
                                                        <td>${d3_format.format(".1%")(internet[1]['y'])}</td>
                                                      </tr>`;
  document.getElementById("access-internet-table2").innerHTML = `<tr>
                                                        <td>$35K-$49.9K</td>
                                                        <td>>50K</td>
                                                      </tr>
                                                      <tr>
                                                        <td>${d3_format.format(".1%")(internet[2]['y'])}</td>
                                                        <td>${d3_format.format(".1%")(internet[3]['y'])}</td>
                                                      </tr>`;

};

// Make the intakes line chart
function makeIntakeChart(d){

  Highcharts.chart('access-intake-chart', {
    chart: {
      type: "scatter",
      renderTo: "chart"
    },
    title: {
      text:  `Percent of SCLS Intakes Accepted`,
      useHTML: true,
      style: {
        color: '#384055',
        fontSize: '.9rem',
        fontFamily: "Copse",
        padding: "0.125rem 1rem",
        fontWeight: 'bold'
      },
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize:'.9rem',
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
          if(this.series.name == "SC Median"){
              return `<p class="popup-header">SC Median</p>
                      <p class="popup-value">${d3_format.format(".1%")(this.x)}`
          } else{
              return `<p class="popup-header">${d[2]}</p>
              <p class="popup-value">${d3_format.format(".1%")(this.x)}</p>
              <p class="popup-context">${d3_format.format(",")(parseFloat(d[0]))} intakes accepted out of ${d3_format.format(",")(parseFloat(d[1]))}</p>
              `
          }
      }
    },
    xAxis: {
      min: 0,
      max: 1,
      labels: {
        formatter: function () {
          return `<p class="profile-chart-category-label">${d3_format.format(".0%")(this.value)}</p>`;
        }
      }
    },
    yAxis: {
      max: 0,
      min : 0,
      minRange :0.1,
      maxPadding: .01,
      lineWidth: 0,
      tickWidth: 2,
      tickLength: 6,
      labels: {
          y: 3
      },
      tickInterval: 1,
      labels: {
        enabled: !1
      },
      title : {
        enabled: false,
      }, 
      tickWidth: 0,
      tickLength: 0,
      maxPadding: 0,
    },
    series: [{
        name: `${d[2]}`,
        color: "#384055",
        data: [[parseFloat(d[0])/parseFloat(d[1]), 0]]}, 
        {
        name: "SC Median",
        color: '#98b1bf',
        data: [[intakeMedian, 0]],
        }]
  });
};

/* Hide content and show the spinner while content loads */
async function contentLoading(loaderID, sectionClass){
  document.getElementById(loaderID).style.display = "block";
  document.getElementById(loaderID).style.visibility = "visible";
  document.querySelectorAll(sectionClass).forEach(function(el) {
    el.style.visibility = 'hidden';
  });
};

/* Hide spinner and show content when it is ready */
async function contentReady(loaderID, sectionClass) {
  window.dispatchEvent(new Event('resize')); 
  document.getElementById(loaderID).style.display = "none";
  document.querySelectorAll(sectionClass).forEach(function(el) {
    el.style.visibility = 'visible';
  });
};

async function stateAttorneyandIntakeStats(){
  // Pull data and add up stats
  let totalAttorneys = 0, totalIntakes = 0, acceptedIntakes = 0, totalPoverty125 = 0;
  await d3_fetch.csv(profileDataUrl).then(dat => { 
    for (let i in dat) {
      if(typeof dat[i]['access_attorneys'] !== 'undefined'){
        totalAttorneys += parseInt(dat[i]['access_attorneys']);
        totalIntakes += parseInt(dat[i]['access_intakes']);
        acceptedIntakes += parseInt(dat[i]['access_accepted_intakes']);
        totalPoverty125 += parseInt(dat[i]['p125']);
      }
    };
  });
  // Attorneys stats
  document.getElementById("access-stats-container").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(",")(totalAttorneys)}</span> attorneys for <span class="banner-text">${d3_format.format(",")(totalPoverty125)}</span> people below 125% of the poverty line</p>`;
  
  // Intakes stats
  document.getElementById("access-intakes-stats").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format(",")(acceptedIntakes)}</span> intakes accepted of <span class="banner-text">${d3_format.format(",")(totalIntakes)}</span> total intakes</p>`;

  // Return stats
  return [totalAttorneys, totalIntakes, acceptedIntakes, totalPoverty125];
};

function stateIntakeChart(intakes, acceptedIntakes){
  Highcharts.chart('access-intake-chart', {
    chart: {
      type: "scatter",
      renderTo: "chart"
    },
    title: {
      text:  `Percent of SCLS Intakes Accepted`,
      useHTML: true,
      style: {
        color: '#384055',
        fontSize: '.9rem',
        fontFamily: "Copse",
        padding: "0.125rem 1rem",
        fontWeight: 'bold'
      },
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize:'.9rem',
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
        return `<p class="popup-header">South Carolina</p>
        <p class="popup-value">${d3_format.format(".1%")(this.x)}</p>
        <p class="popup-context">${d3_format.format(",")(acceptedIntakes)} intakes accepted out of ${d3_format.format(",")(intakes)}</p>
        `
        }
    },
    xAxis: {
      min: 0,
      max: 1,
      labels: {
        formatter: function () {
          return `<p class="profile-chart-category-label">${d3_format.format(".0%")(this.value)}</p>`;
        }
      }
    },
    yAxis: {
      max: 0,
      min : 0,
      minRange :0.1,
      maxPadding: .01,
      lineWidth: 0,
      tickWidth: 2,
      tickLength: 6,
      labels: {
          y: 3
      },
      tickInterval: 1,
      labels: {
        enabled: !1
      },
      title : {
        enabled: false,
      }, 
      tickWidth: 0,
      tickLength: 0,
      maxPadding: 0,
    },
    series: [{
        name: "South Carolina",
        color: '#384055',
        data: [[(acceptedIntakes/intakes), 0]],
        }]
  });
};

function stateAttorneyChart(attorneys, totalPoverty){

  Highcharts.chart('access-attorney-chart', {
    chart: {
      type: "scatter",
      renderTo: "chart"
    },
    title: {
      align: 'center',
      text:  `Attorneys per 1,000 People Below 125% of the Poverty Line`,
      useHTML: true,
      style: {
        color: '#384055',
        fontSize: '.9rem',
        fontFamily: "Copse",
        padding: "0.125rem 1rem",
        fontWeight: 'bold'
      },
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize:'.9rem',
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
        return `<p class="popup-header">South Carolina</p>
        <p class="popup-value">${d3_format.format(",.1f")(this.x)}</p>
        <p class="popup-context">${d3_format.format(",")(attorneys)} attorneys for ${d3_format.format(",")(totalPoverty)} people below 125% poverty.</p>`;
      }
    },
    xAxis: {
      min: 0.95*(attorneys/(parseFloat(totalPoverty)/1000)),
      max: 1.05*(attorneys/(parseFloat(totalPoverty)/1000)),
      labels: {
        formatter: function () {
          return `<p class="profile-chart-category-label">${d3_format.format(".3")(this.value)}</p>`;
        }
      }
    }, 
    yAxis: {
      max: 0,
      min : 0,
      minRange :0.1,
      maxPadding: .01,
      lineWidth: 0,
      tickWidth: 2,
      tickLength: 6,
      labels: {
          y: 3
      },
      tickInterval: 1,
      labels: {
        enabled: !1
      },
      title : {
        enabled: false,
      }, 
      tickWidth: 0,
      tickLength: 0,
      maxPadding: 0,
    },
    series: [{
        name: `South Carolina`,
        color: "#384055",
        data: [[(attorneys/(totalPoverty/1000)), 0]],
      }]
  });
};

async function makeStateAccessSection(){

  // Show subheadings
  document.getElementById("access-attorneys").innerHTML = `Attorneys`;
  document.getElementById("access-intakes").innerHTML = `Intakes`;

  // Attorney and intake stats
  let response = await stateAttorneyandIntakeStats();
  let attorneys = response[0];
  let intakes = response[1];
  let acceptedIntakes = response[2];
  let totalPoverty = response[3];

  // Wait for all content to load before hiding the spinner
  Promise.all([displayInternetAccess("statewide", "none"), stateIntakeChart(intakes, acceptedIntakes), stateAttorneyChart(attorneys, totalPoverty)]).then(() => {
    contentReady("access-loader", ".profile-access");
  });
  
};

function statePlaintiffDefendantRepChart(data){
  Highcharts.chart('profile-rep-comp-chart', {
    chart: {
      type: 'bar',
      zoomType: 'y',
      height: '400px'
    },
    plotOptions: {
      series: {
          minPointLength: 2
      }
    },
    title: {
      text: undefined,
    },
    legend : {
      useHTML: true,
      itemStyle: {
        fontSize:'.9rem',
      }
    },
    tooltip : {
      useHTML: true,
      outside: true,
      backgroundColor: '#FFFFFF',
      formatter: function() {
        return `<p class="popup-header">${this.series.name}</p>
                <p class="popup-value">${this.series.userOptions.data[this.point.index]['type']}</p>
                <p class="popup-context">${d3_format.format(".1%")(this.y)} unrepresented</p>`;
      }
    },
    xAxis : {
      categories: ["Debt Collection", "Foreclosure", "Claim & Delivery (Circuit Court)", "Claim & Delivery (Magistrate Court)", "Eviction"],
      labels: {
          useHTML: true,
          style: {
              width:'100px',
          },
          formatter: function () {
              return `<p class="profile-chart-category-label">${this.value}</p>`;
          }
      }
    },
    yAxis: {
      max: 1,
      title: {
        useHTML: true,
        text: '% Unrepresented',
        style: {
          fontSize: '1rem',
          fontFamily: "Segoe UI",
        },
      },
      labels: {
          formatter: function () {
              return `<p class="profile-chart-category-label">${this.value*100}%</p>`;
          }
      }
    },
    series: [{
      color: '#98b1bf',
      name: "Plaintiffs",
      data: [
        {'type': 'Debt Collection', 'y': data['debt'][1]},
        {'type': 'Foreclosure', 'y': data['foreclosure'][1]},
        {'type': 'Claim & Delivery (Circuit Court)', 'y': data['circuit'][1]},
        {'type': 'Claim & Delivery (Magistrate Court)', 'y': data['magistrate'][1]},
        {'type': 'Eviction', 'y': data['eviction'][1]}
      ]
    },{
      color: '#384055',
      name: `Defendants`,
      data: [
        {'type': 'Debt Collection', 'y': data['debt'][0]},
        {'type': 'Foreclosure', 'y': data['foreclosure'][0]},
        {'type': 'Claim & Delivery (Circuit Court)', 'y': data['circuit'][0]},
        {'type': 'Claim & Delivery (Magistrate Court)', 'y': data['magistrate'][0]},
        {'type': 'Eviction', 'y': data['eviction'][0]}
      ]}]  
  }); 
}

async function stateRepresentationData(){
  // Pull data and add up stats
  let evictionCases = 0, defendantsUnrepEviction = 0, plaintiffsUnrepEviction = 0;
  let debtCases = 0, defendantsUnrepDebt = 0, plaintiffsUnrepDebt = 0;
  let foreclosureCases = 0, defendantsUnrepForeclosure = 0, plaintiffsUnrepForeclosure = 0;
  let circuitCases = 0, defendantsUnrepCircuit = 0, plaintiffsUnrepCircuit = 0;
  let magistrateCases = 0, defendantsUnrepMagistrate = 0, plaintiffsUnrepMagistrate = 0;

  await d3_fetch.csv(profileDataUrl).then(dat => { 
    for (let i in dat) {
      if(typeof dat[i]['rep_eviction_num_cases'] !== 'undefined'){
        // Eviction
        evictionCases += parseInt(dat[i]['rep_eviction_num_cases']);
        defendantsUnrepEviction += parseInt(dat[i]['rep_eviction_def_unrep']);
        plaintiffsUnrepEviction += parseInt(dat[i]['rep_eviction_plaint_unrep']);
        // Debt collection
        debtCases += parseInt(dat[i]['rep_debt_num_cases']);
        defendantsUnrepDebt += parseInt(dat[i]['rep_debt_def_unrep']);
        plaintiffsUnrepDebt += parseInt(dat[i]['rep_debt_plaint_unrep']);
        // Foreclosure
        foreclosureCases += parseInt(dat[i]['rep_foreclosure_num_cases']);
        defendantsUnrepForeclosure += parseInt(dat[i]['rep_foreclosure_def_unrep']);
        plaintiffsUnrepForeclosure += parseInt(dat[i]['rep_foreclosure_plaint_unrep']);
        // Circuit
        circuitCases += parseInt(dat[i]['rep_circ_num_cases']);
        defendantsUnrepCircuit += parseInt(dat[i]['rep_circ_def_unrep']);
        plaintiffsUnrepCircuit += parseInt(dat[i]['rep_circ_plaint_unrep']);
        // Magistrate
        magistrateCases += parseInt(dat[i]['rep_mag_num_cases']);
        defendantsUnrepMagistrate += parseInt(dat[i]['rep_mag_def_unrep']);
        plaintiffsUnrepMagistrate += parseInt(dat[i]['rep_mag_plaint_unrep']);
      }
    };
  });

  // Return stats for each topic with defendant unrepresented, plaintiff unrepresented
  let chartResult = {
    'eviction': [defendantsUnrepEviction/evictionCases, plaintiffsUnrepEviction/evictionCases],
    'debt': [defendantsUnrepDebt/debtCases, plaintiffsUnrepDebt/debtCases],
    'foreclosure': [defendantsUnrepForeclosure/foreclosureCases, plaintiffsUnrepForeclosure/foreclosureCases],
    'circuit': [defendantsUnrepCircuit/circuitCases, plaintiffsUnrepCircuit/circuitCases],
    'magistrate': [defendantsUnrepMagistrate/magistrateCases, plaintiffsUnrepMagistrate/magistrateCases]
  };

  let statResult = {
    'total cases': evictionCases + debtCases + foreclosureCases + circuitCases + magistrateCases,
    'total defendants unrepresented': defendantsUnrepEviction + defendantsUnrepDebt + defendantsUnrepForeclosure + defendantsUnrepCircuit + defendantsUnrepMagistrate,
    'total plaintiffs unrepresented': plaintiffsUnrepEviction + plaintiffsUnrepDebt + plaintiffsUnrepForeclosure + plaintiffsUnrepCircuit + plaintiffsUnrepMagistrate,
  };

  return [chartResult, statResult];
};

async function stateRepStats(stats){
  // Show stats
  document.getElementById("rep-stat-intro").innerHTML = `<p class="banner-heading">Across all case types there were <span class="banner-text">${d3_format.format(",")(stats['total cases'])}</span> cases, <span class="banner-text">${d3_format.format(",")(stats['total defendants unrepresented'])}</span> with unrepresented defendants and <span class="banner-text">${d3_format.format(",")(stats['total plaintiffs unrepresented'])}</span> with unrepresented plaintiffs.</p>`;
  document.getElementById("rep-stat-def").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format('.1%')(stats['total defendants unrepresented']/stats['total cases'])}</span> of cases had unrepresented defendants.</p>`;
  document.getElementById("rep-stat-plaint").innerHTML = `<p class="banner-heading"><span class="banner-text">${d3_format.format('.1%')(stats['total plaintiffs unrepresented']/stats['total cases'])}</span> of cases had unrepresented plaintiffs.</p>`;
  
  // Show rep items relating to county only profiles
  document.querySelectorAll('.rep-state-only').forEach(function(el) {
    el.style.display = 'block';
  });
};

async function makeStateRepSection(){
  // Hide rep items relating to county only profiles
  document.querySelectorAll('.rep-county-only').forEach(function(el) {
    el.style.visibility = 'hidden';
  });

  // Section names
  document.getElementById("rep-plaint-def").innerHTML = `Percent Unrepresented by Case Type`;

  // Collect statewide data
  let response = await stateRepresentationData();

  // Plaintiff vs defendant representation chart
  Promise.all([statePlaintiffDefendantRepChart(response[0]), stateRepStats(response[1])]).then(() => {
    contentReady("rep-loader", ".profile-rep");
  });

};

async function loadStateProfile(){
  // Get year from google sheet
  await d3_fetch.csv(profileDataUrl).then(dat => { 
    year = dat[0]['year'];
  });
  // Make need section, specifying state instead of county
  makeNeedSection("none", "none", "statewide");
  // Make state access section
  makeStateAccessSection();
  // Make representation section
  makeStateRepSection();
};

// This function is called to build the profile page when the page first loads and when a different county is selected
async function loadProfile(county){

  let dataQuery = d3_fetch.csv(profileDataUrl);
  let countyData = await dataQuery.then(dat => { 
    year = dat[0]['year'];
    return dat.filter(d => d.county === county);
  });

  document.getElementById("county-profile-name").innerHTML = `${county} County`;
  document.querySelectorAll(".county-profile-img").forEach(function(el) {
    el.style.display = 'none';
  });
  document.getElementById(`profile-img-${county}`).style.display = 'block';

  makeNeedSection(countyData[0]['fips'], county, "county");
  makeAccessSection(countyData);
  makeRepSection(countyData);
};

// This function maintains the profile page
export default async function profile() {

  // Underline only the current nav item in navbar
  document.getElementById("nav-item-mapbar").style.textDecoration = "none";
  document.getElementById("nav-item-home").style.textDecoration = "none";
  document.getElementById("nav-item-profiles").style.textDecoration = "underline";

  // Show only county profile section
  changeLayout('page-layout-profiles');
  
  // Dropdown functionality on narrow screen
	document.getElementById("toggle-topic-profile").addEventListener("change", function () {
		if (this.value == "need") {
			document.getElementById("wrapper-profile-need").style.display = "block";
			document.getElementById("wrapper-profile-access").style.display = "none";
			document.getElementById("wrapper-profile-rep").style.display = "none";
    } else if (this.value == "access") {
      document.getElementById("wrapper-profile-need").style.display = "none";
      document.getElementById("wrapper-profile-access").style.display = "block";
      document.getElementById("wrapper-profile-access").style.maxWidth = "100%";
      document.getElementById("wrapper-profile-access").style.width = "100%";
			document.getElementById("wrapper-profile-rep").style.display = "none";
    } else {
      document.getElementById("wrapper-profile-need").style.display = "none";
			document.getElementById("wrapper-profile-access").style.display = "none";
      document.getElementById("wrapper-profile-rep").style.display = "block";
      document.getElementById("wrapper-profile-rep").style.maxWidth = "100%";
      document.getElementById("wrapper-profile-rep").style.width = "100%";
    };
    window.dispatchEvent(new Event('resize')); 
	});

  // First time page is visited, set up the profile
  if(firstVisit){

    // Set flag to false so following code doesn't have to rerun later
    firstVisit = false;

    // Statewide profile header
    document.getElementById("county-profile-name").innerHTML = `Statewide Profile`;
    document.querySelectorAll(".county-profile-img").forEach(function(el) {
      el.style.display = 'none';
    });

    // Show instructions
    document.getElementById("county-profile-instructions").innerHTML = `You are viewing a profile for the state of South Carolina. Select a county in the dropdown above to view its county profile.`;

    // Populate the dropdown from the Google sheet
    await populateDropdown();

    // Show profile sections
    document.querySelectorAll(".profile-content").forEach(function(el) {
      el.style.visibility = 'visible';
    });  

    // If statewide is selected, load statewide profile
    if(selectDropdownJS.value === "Statewide"){
      // Show loaders
      contentLoading("need-loader", ".profile-need");
      contentLoading("access-loader", ".profile-access");
      contentLoading("rep-loader", ".profile-rep");
      loadStateProfile();
    };

    // Calculate median values to display on the profile
    await calculateMedians();

    // Event listener for the county dropdown
    selectDropdownJS.onchange = function(){ 

      // If statewide is selected, load statewide profile
      if(this.value === "Statewide"){
        // Statewide profile header and profile instructions
        document.getElementById("county-profile-name").innerHTML = `Statewide Profile`;
        document.querySelectorAll(".county-profile-img").forEach(function(el) {
          el.style.display = 'none';
        });
        
        document.getElementById("county-profile-instructions").style.display = "block";
        // Show loaders
        contentLoading("need-loader", ".profile-need");
        contentLoading("access-loader", ".profile-access");
        contentLoading("rep-loader", ".profile-rep");
        loadStateProfile();
      } else{
          // Hide instructions 
          document.getElementById("county-profile-instructions").style.display = "none";  

          // Show loaders
          contentLoading("need-loader", ".profile-need");
          contentLoading("access-loader", ".profile-access");
          contentLoading("rep-loader", ".profile-rep");

          // Load profile
          loadProfile(this.value); 
      };
    };
  };
};
