
/**
 * Show or hide the relevant section from within the HTML
 */
function getLayoutSections() {
	return document.getElementsByClassName('page-layout');
}
// export function changeLayout(layoutID) {
// 	for (let i = 0; i < getLayoutSections().length; i++) {
// 		getLayoutSections()[i].style.display = "none"; 
// 	}
// 	document.getElementById(layoutID).style.display = "block";
// }
export function changeLayout(layoutID) {
  for (let i = 0; i < getLayoutSections().length; i++) {
    let item = getLayoutSections()[i]
    if (item.id === 'page-layout-map-bars') {
      item.style.visibility = "hidden";
      item.style['pointer-events'] = "none";
    } else {
      item.style.display = "none";
    }
  }
  if (layoutID === 'page-layout-map-bars') {
    document.getElementById(layoutID).style.visibility = "visible";
    document.getElementById(layoutID).style['pointer-events'] = "auto";
  } else {
    document.getElementById(layoutID).style.display = "block";
  }
}