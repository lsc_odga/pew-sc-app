import { changeLayout } from './changeLayout.js';
import { drawMapboxMap, addCountyBoundaries, mapHasBeenDrawn, countyBoundariesHaveBeenDrawn, scCoords } from '../viz/loadMapboxMap.js';
import { calculateLinearColorScale } from '../d3-utils/d3-utils.js';
import { identifyFeatures, setChroplethColor } from '../viz/mapUtils.js';
import { showPopup, hidePopup, setPopupContents } from '../d3-utils/popup.js';

const d3_scale = require('d3-scale');
const d3_array = require('d3-array');
const d3_selection = require('d3-selection');
const d3_axis = require('d3-axis');
const d3_transition = require('d3-transition');
const d3_format = require('d3-format');
const d3_fetch = require('d3-fetch');

const percentageVars = ['per_def_unrep', 'per_plaint_unrep', 'per_neither_rep','per_p100', 'per_p125', 'per_accepted']; 
const decimalVars = ['per_attorney100', 'per_attorney125', 'per_accepted100', 'per_accepted125', 'num_cases_per_capita', 'def_unrep_per_capita', 'plaint_unrep_per_capita'];
const variableNameLookup = {
  'num_cases': 'Number of cases', 
	'neither_rep': 'Number of cases with neither party represented',
  'per_neither_rep': 'Percent of cases with neither party represented',
  'def_unrep': 'Number of cases with unrepresented defendants',
	'per_def_unrep': 'Percent of cases with unrepresented defendants',
  'plaint_unrep': 'Number of cases with unrepresented plaintiffs', 
  'per_plaint_unrep': 'Percent of cases with unrepresented plaintiffs',
  'p100': 'Number of people below 100% poverty',
	'p125': 'Number of people below 125% poverty',
  'per_p100': 'Percent of people below 100% poverty',
  'per_p125': 'Percent of people below 125% poverty',
  'intakes': 'Total intakes', 
  'accepted': 'Accepted intakes', 
  'per_accepted': 'Percent of intakes accepted',
  'per_accepted100': 'Accepted intakes per 1,000 below 100% poverty', 
  'per_accepted125': 'Accepted intakes per 1,000 below 125% poverty', 
  'attorneys': 'Total attorneys',
  'per_attorney100': 'Private attorneys per 1,000 people below 100% poverty',
  'per_attorney125': 'Private attorneys per 1,000 people below 125% poverty',
  'num_cases_per_capita': 'Number of cases per 1,000 below 125% poverty',
	'def_unrep_per_capita': 'Number of cases with unrepresented defendants per 1,000 below 125% poverty',
	'plaint_unrep_per_capita': 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty',
};

let map, activeData, selectedVar, selectedYear, selectedSort = 'descend', colorScale, legend;
let barGraphContainer, width, height, barHeight, margins, chartAreaDimensions, barsSvg, barsAreaG, xScale, axisTopG, yScale, axisLeftG, bars, labels, labelPadding, sortedCounties, eventListenersAdded;
let fipsToCounty = {}, popupValueLookup = {}, numCasesLookup = {}, defUnRepLookup = {}, plaintUnRepLookup = {}, neitherRepLookup ={}, poverty100 = {}, poverty125 = {}, totalPop = {}, acceptedLookup = {}, attorneyLookup = {}, intakeLookup = {}, counties=[];
let varsToCalculate = [];
let newConfig;
let missingFlag = false;
                                      
export default function drawMapBars(config) {

  // Use newest config
  newConfig = config;
  
  // Underline only the current nav item in navbar
  document.getElementById("nav-item-mapbar").style.textDecoration = "underline";
  document.getElementById("nav-item-home").style.textDecoration = "none";
  document.getElementById("nav-item-profiles").style.textDecoration = "none";

  document.getElementById("description-placeholder").style.opacity = 0;
  // Get the description from Google sheeets
  let captionsURL = "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=MapBars Captions";
  d3_fetch.csv(captionsURL).then(dat => {
    let description = document.getElementById("description-placeholder");
    description.innerHTML = dat[0][newConfig.title2];
    description.style.opacity = 1;
  });
  
	/*------------ PAGE LAYOUT -----------*/
	let colorGradient = ['#bbd4fc', '#4D5C74'];

  //Set layout to mapbars and update titles
  changeLayout('page-layout-map-bars');
	document.getElementById('main-page-title').innerHTML = newConfig.title;
	document.getElementById('main-page-title2').innerHTML = newConfig.title2;

  //Data url from config
  let dataUrl = newConfig.dataUrl;

  //Find the possible years for this topic
  async function findYears(dataUrl){
    let csvData = d3_fetch.csv(dataUrl);
    let data = csvData.then(dat => {
      //Find years from the data
      const years = [...new Set(dat.map( d => d['Year'] ) )];
      return years;
    });
    return data;
  };

  //Put the years in the dropdown menu
  findYears(dataUrl).then(years => {
    const yearSort = Array.from(years).sort(function(a, b){
      return b - a;
    });
    const yearsSelect = d3_selection.select("#map-bars-year-select");
    yearsSelect.selectAll("option").remove();
    yearsSelect.selectAll("option")
      .data(yearSort)
      .enter()
        .append("option")
        .attr("value", function (d) { return d; })
        .text(function (d) { return d });

    //If first time or the selectedYear variable isn't available in the new list of years then use the first year in the list
    if ( !selectedYear || yearSort.indexOf(selectedYear) === -1 ) { 
      yearsSelect.property('value', yearSort[0]);
      selectedYear = yearsSelect.node().value;
    //Otherwise continue using the previously selected year
    }	else {
      yearsSelect.property('value', selectedYear);
    }
  });

  //Show the variables to display in the dropdown
  let varsSelect = d3_selection.select("#map-bars-var-select");
  let vars = newConfig.displayVariables;
  let varsValues = d3_array.map(vars, function(d) { return d.value });

  varsSelect.selectAll("option").remove();
  varsSelect.selectAll("option")
    .data(vars)
    .enter()
      .append("option")
      .attr("value", function (d) { return d.value })
      .text(function (d) { return d.text });

  //Keep variable consistent across topic changes if possible
  if ( !selectedVar || varsValues.indexOf(selectedVar) === -1 ) { 
    varsSelect.property('value', varsValues[0]);
    selectedVar = varsSelect.node().value;
  }	else {
    varsSelect.property('value', selectedVar);
  }	

  /* Load the data and save certain values for popups */
  async function getData(dataUrl){

    let csvData = d3_fetch.csv(dataUrl);
    let data = await csvData.then(dat => {

      //Get all variables from config
      let variables = [];
      for(let obj of newConfig.variables){
        variables.push(obj.value);
      };
      for(let obj of newConfig.displayVariables){
        variables.push(obj.value);
      };
      for(let obj of newConfig.calculateVariables){
        variables.push(obj.value);
      };

      //Filter for current data;
      dat = dat.filter(d => d['Year'] === selectedYear);

      //Get calculated variables from config
			varsToCalculate = [];
			let calculateVars = newConfig.calculateVariables;
			for( let i in calculateVars){
				varsToCalculate.push(calculateVars[i].value)
			}

      //Clear counties variable
      counties = [];

			//Create some lookup dictionaries for popup content rather then filtering all the data on every tick of the mousemove
			for (let i in dat) {

				var obj = dat[i];

        //Make any empty entries null
        for (const [key, value] of Object.entries(obj)) {
          if(value == "" && key != ""){
            obj[key] = NaN;
          };
        };
				
				//Representation lookup tables and calculated variables
				if(varsToCalculate.some(variable => ['def_unrep', 'plaint_unrep', 'per_def_unrep', 'per_plaint_unrep', 'per_neither_rep'].includes(variable))){
					//vars
          obj.def_unrep = obj.num_cases - obj.both_rep - obj.def_rep;
          obj.plaint_unrep = obj.num_cases - obj.both_rep - obj.plaint_rep;
					obj.per_def_unrep =  obj.def_unrep / obj.num_cases; 
					obj.per_plaint_unrep =  obj.plaint_unrep / obj.num_cases; 
					obj.per_neither_rep =  obj.neither_rep / obj.num_cases; 
          obj.num_cases_per_capita = obj.num_cases / (obj.p125/1000);
          obj.def_unrep_per_capita = obj.def_unrep / (obj.p125/1000);
          obj.plaint_unrep_per_capita = obj.plaint_unrep / (obj.p125/1000);

					//lookup
					defUnRepLookup[String(dat[i].county)] = dat[i]['def_unrep'];
					plaintUnRepLookup[String(dat[i].county)] = dat[i]['plaint_unrep'];
					neitherRepLookup[String(dat[i].county)] = dat[i]['neither_rep'];
					numCasesLookup[String(dat[i].county)] = dat[i]['num_cases'];
          poverty125[String(dat[i].county)] = dat[i]['p125'];
				};

				//Need lookup tables and calculated variables
				if(varsToCalculate.some(variable => ['per_p100', 'per_p125'].includes(variable))){
					//vars
					obj.per_p100 = obj.p100 / obj.population;
					obj.per_p125 = obj.p125 / obj.population;
					//lookup
					poverty100[String(dat[i].county)] = dat[i]['p100'];
					poverty125[String(dat[i].county)] = dat[i]['p125'];
					totalPop[String(dat[i].county)] = dat[i]['population'];
				};
				
				//Access lookup tables and calculated variables
				if(varsToCalculate.some(variable => ['per_accepted', 'per_accepted100', 'per_accepted125', 'per_attorney100', 'per_attorney125'].includes(variable))){
					//vars
					obj.per_accepted = obj.accepted / obj.intakes;
					obj.per_accepted100 = obj.accepted / (obj.p100/1000);
					obj.per_accepted125 = obj.accepted / (obj.p125/1000);
					obj.per_attorney100 = obj.attorneys / (obj.p100/1000);
					obj.per_attorney125 = obj.attorneys / (obj.p125/1000);
					//lookup
					poverty100[String(dat[i].county)] = dat[i]['p100'];
					poverty125[String(dat[i].county)] = dat[i]['p125'];
					acceptedLookup[String(dat[i].county)] = dat[i]['accepted'];
					intakeLookup[String(dat[i].county)] = dat[i]['intakes'];
					attorneyLookup[String(dat[i].county)] = dat[i]['attorneys'];
				};

				//Convert number values to numbers (they come as strings from the CSV)
        obj['plaint_unrep'] = +obj['plaint_unrep'];
				obj['p100'] = +obj['p100'];
				obj['p125'] = +obj['p125'];
				obj['intakes'] = +obj['intakes'];
				obj['accepted'] = +obj['accepted'];
				obj['attorneys'] = +obj['attorneys'];
        obj['def_unrep'] = +obj['def_unrep'];
				obj['neither_rep'] = +obj['neither_rep'];
				obj['num_cases'] = +obj['num_cases'];
        obj['both_rep'] = +obj['both_rep'];

				//Lookup tables that are always required
        counties.push(dat[i].county);
				fipsToCounty[String(dat[i].fips)] = dat[i].county;
				popupValueLookup[String(dat[i].fips)] = dat[i][selectedVar];
			}
      return dat;  
    })
    return data;
  };

  /* Update the sort of the bar chart */
  function updateSort(){
		//Maintain the bar chart sort option
    let sortSelect = d3_selection.select("#map-bars-sort-select");
    selectedSort = sortSelect.property('value');

		//Update sort when changes happen
    //Alphabetical
		if(selectedSort == 'alpha'){
			let countySortAlpha = function(a, b) { //First sorts nulls to the bottom, then by value, then by name (for nulls in county name order)
      return d3_array.ascending(isNaN(a[selectedVar]), isNaN(b[selectedVar])) ||
					d3_array.ascending(a['county'], b['county']) || 
					d3_array.ascending(a['county'], b['county'])
			}
			sortedCounties = activeData.sort(countySortAlpha);	
		}
    //Ascending
		if(selectedSort == 'ascend'){
			let countySortAscend = function(a, b) { //First sorts nulls to the bottom, then by value, then by name (for nulls in county name order)
      return d3_array.ascending(isNaN(a[selectedVar]), isNaN(b[selectedVar])) ||
					d3_array.ascending(a[selectedVar], b[selectedVar]) || 
					d3_array.ascending(a['county'], b['county'])
			}
			sortedCounties = activeData.sort(countySortAscend);
		}
    //Descending
		if(selectedSort == 'descend'){
			let countySortDescend = function(a, b) { //First sorts nulls to the bottom, then by value, then by name (for nulls in county name order)
      return d3_array.ascending(isNaN(a[selectedVar]), isNaN(b[selectedVar])) ||
					d3_array.descending(a[selectedVar], b[selectedVar]) || 
					d3_array.ascending(a['county'], b['county'])
			}
			sortedCounties = activeData.sort(countySortDescend);
    }
    //Apply sort to the bar chart
		yScale.domain(d3_array.map(sortedCounties, function(d) { return d.county; }));			
		barsAreaG.selectAll('rect').transition().duration(2000)
			.attr('y', function(d) { return yScale(d['county'])})
			.attr('width', function(d) { return xScale(d[selectedVar] )})
			.attr('fill', function(d) { return colorScale(d[selectedVar]) });
		let yAxis = d3_axis.axisLeft(yScale);
		axisLeftG.transition().duration(2000).call(yAxis);
		barsAreaG.selectAll('.bar-label')
			.text(function(d) { 
				if(isNaN(d[selectedVar])){
					return 'No data';
				}
				if(percentageVars.includes(selectedVar)){
					return d3_format.format("0.1%")(d[selectedVar]);
				} else if(decimalVars.includes(selectedVar)){
					return d3_format.format(",.1f")(d[selectedVar]);
				} else{
					return d3_format.format(",")(d[selectedVar]);
				};
			}) 
			.attr('y', function(d) { return yScale(d['county']) + yScale.bandwidth() / 2 + 5 })
			.attr('x', function(d) { return (xScale(d[selectedVar]) || 0) + labelPadding });
		barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(0).duration(0).style('opacity', 0);
		barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(2000).duration(800).style('opacity', 1);
	};

  //Load empty map if not already loaded
	async function drawBaseMap() {
		if (!map) {
			map = await drawMapboxMap([-81.0, 33.6], 6.9);
			return map;
		} else {
			return map;
		}
	};

  	/*------------ D3 SETUP AND DEFINITIONS -----------*/
	//First time only
	if (!barsSvg) {
		barGraphContainer = d3_selection.select('#bar-graph');
    	width = barGraphContainer.node().getBoundingClientRect().width;
    	height = 1200;
    	barHeight = height / 45;
		margins = { top: 40, right: 70, bottom: 20, left: 100};
		chartAreaDimensions = { width: width - margins.left - margins.right, height: height - margins.top - margins.bottom};

		barsSvg = d3_selection.select('#bar-graph').append('svg')
			.attr('width', width)
			.attr('height', height)
			.attr('id', 'bar-svg');

		barsAreaG = barsSvg.append('g')
			.attr('id', 'bars-area-g')
			.attr('transform', `translate(${margins.left}, ${margins.top})`);
		
		xScale = d3_scale.scaleLinear().range([0, chartAreaDimensions.width]);
		axisTopG = barsAreaG.append('g').attr('id', 'axis-top-g');

		yScale = d3_scale.scaleBand().range([0, chartAreaDimensions.height]).padding(0.1);
		axisLeftG = barsAreaG.append('g').attr('id', 'axis-left-g');
	};

  Promise.all([drawBaseMap(), getData(dataUrl)])
  .then((responses) => {
  
    activeData = responses[1];

    /*--------- SHARED ELEMENTS LIKE COLOR SCALES ---------*/
    colorScale = calculateLinearColorScale(activeData, selectedVar, colorGradient);

    let mapPopup = d3_selection.select('#map-popup');
    let popup = d3_selection.select('#popup');

    /*--------- MAP ---------*/
    if (!map.loaded()) {
      map.on('load', (e) => {
        if (!countyBoundariesHaveBeenDrawn) { addCountyBoundaries(map); }
        console.log("A2", countyBoundariesHaveBeenDrawn);
        setChroplethColor(map, activeData, selectedVar, colorScale);
        //Not until this point allow the select menu to change variables
        document.getElementById('map-bars-var-select').disabled = false;
        document.getElementById('map-bars-year-select').disabled = false;
      });
    } else {
      if (!countyBoundariesHaveBeenDrawn) { addCountyBoundaries(map); }
      setChroplethColor(map, activeData, selectedVar, colorScale);
      document.getElementById('map-bars-var-select').disabled = false;
    };
    
    /*--------- D3 BAR GRAPH ---------*/		

    // To start, sort the bar chart ascending
    let countySort = function(a, b) { //First sorts nulls to the bottom, then by value, then by name (for nulls in county name order)
        return d3_array.ascending(isNaN(a[selectedVar]), isNaN(b[selectedVar])) ||
          d3_array.descending(a[selectedVar], b[selectedVar]) || 
          d3_array.ascending(a['county'], b['county'])
    }

    //X AXIS
    xScale.domain([0, d3_array.max(activeData, function(d) { return d[selectedVar]; })]).nice();
    let xAxis = d3_axis.axisTop(xScale).ticks(2).tickSize(-chartAreaDimensions.height); 
    if(percentageVars.includes(selectedVar)){
      xAxis.tickFormat(d3_format.format(".1%"));
    } else{
      xAxis.tickFormat(d3_format.format(",.0f"));
    }
    axisTopG.transition().duration(2000).call(xAxis);
    axisTopG.selectAll('.tick line').style("stroke-dasharray", ("3, 3"));

    //Y AXIS
    sortedCounties = activeData.sort(countySort);			
    yScale.domain(d3_array.map(sortedCounties, function(d) { return d.county; }));			
    let yAxis = d3_axis.axisLeft(yScale);
    axisLeftG.transition().duration(2000).call(yAxis);

    //BARS
    function barsAndLabels() {
      bars = barsAreaG.selectAll('rect');
      labels = barsAreaG.selectAll('.bar-label');
      labelPadding = 5;

      bars
        .data(activeData, function(d) { return d.county }) //To take the new data (i.e. when switching from debt to eviction) we need to use a KEY in .data(data, KEY) so that the data are bound to the right existing element.
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr('id', function(d) { return `bar-${String(d.fips)}`})
        .attr('x', 0)
        .attr('height', yScale.bandwidth())
        .attr('y', function(d) { return yScale(d['county']) });

      barsAreaG.selectAll('rect').transition().duration(2000) //not sure why it's not liking "bars" here and we need barsAreaG.selectAll('rect') instead :/
        .attr('y', function(d) { return yScale(d['county']) })
        .attr('width', function(d) { return xScale(d[selectedVar] )})
        .attr('fill', function(d) { return colorScale(d[selectedVar]) });

      labels
        .data(activeData, function(d) { return d.county })
        .enter()
        .append('text')
        .attr('class', 'bar-label')
        .attr('id', function(d) { return `bar-label-${String(d.fips)}` })
        .attr('x', 0);

      barsAreaG.selectAll('.bar-label')
        .text(function(d) { 
          if(isNaN(d[selectedVar])){
            return 'No data';
          }
          if(percentageVars.includes(selectedVar)){
            return d3_format.format("0.1%")(d[selectedVar]);
          } else if(decimalVars.includes(selectedVar)){
            return d3_format.format(",.1f")(d[selectedVar]);
          } else{
            return d3_format.format(",")(d[selectedVar]);
          };
        }) 
        .attr('y', function(d) { return yScale(d['county']) + yScale.bandwidth() / 2 + 5 })
        .attr('x', function(d) { return (xScale(d[selectedVar]) || 0) + labelPadding });

      barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(0).duration(0).style('opacity', 0);
      barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(2000).duration(800).style('opacity', 1);

      //If no data is missing, missingFlag is false. This is so the legend won't show a color for missing values.
      missingFlag = false;
      //Set missingFlag to true if any data is missing. This makes the legend show grey for missing values.
      for(let row in activeData){
        if(isNaN(activeData[row][selectedVar])){
          missingFlag = true;
        };
      };

	    //Create the legend
	    updateLegend();
	  };

    barsAndLabels();
    updateSort();

    /*--------- WHEN THE SELECTED VARIABLE OR YEAR CHANGES ---------*/

    function selectUpdate() {
    
      popupValueLookup = {};
      for (let i in activeData) {
        popupValueLookup[String(activeData[i].fips)] = activeData[i][selectedVar];
      }

      /* ----- MAP ----- */
      colorScale = calculateLinearColorScale(activeData, selectedVar, colorGradient);
      setChroplethColor(map, activeData, selectedVar, colorScale);

      /* ----- BAR GRAPH ----- */
      sortedCounties = activeData.sort(countySort);	
      xScale.domain([0, d3_array.max(activeData, function(d) { return d[selectedVar]; })]).nice();	
      if(percentageVars.includes(selectedVar)){
        xAxis.tickFormat(d3_format.format(".1%"));
      } else{
        xAxis.tickFormat(d3_format.format(",.0f"));
      }	
      yScale.domain(d3_array.map(sortedCounties, function(d) { return d.county; }));

      axisLeftG.transition().duration(2000).call(yAxis);
      axisTopG.transition().duration(2000).call(xAxis);

      barsAreaG.selectAll('rect').transition().duration(2000)
        .attr('y', function(d) { return yScale(d['county']) })
        .attr('width', function(d) { return xScale(d[selectedVar] )})
        .attr('fill', function(d) { return colorScale(d[selectedVar]) });

      barsAreaG.selectAll('.bar-label')
        .text(function(d) { 
          if(isNaN(d[selectedVar])){
            return 'No data';
          }
          if(percentageVars.includes(selectedVar)){
            return d3_format.format("0.1%")(d[selectedVar]);
          } else if(decimalVars.includes(selectedVar)){
            return d3_format.format(",.1f")(d[selectedVar]);
          } else{
            return d3_format.format(",")(d[selectedVar]);
          };
        }) 
        .attr('x', function(d) { return (xScale(d[selectedVar]) || 0) + labelPadding })
        .attr('y', function(d) { return yScale(d['county']) + yScale.bandwidth() / 2 + 5 });
      
      barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(0).duration(0).style('opacity', 0);
      barsAreaG.selectAll('.bar-label').transition('barlabelopacity').delay(2000).duration(800).style('opacity', 1);

      //If no data is missing, missingFlag is false. This is so the legend won't show a color for missing values.
      missingFlag = false;
      //Set missingFlag to true if any data is missing. This makes the legend show grey for missing values.
      for(let row in activeData){
        if(isNaN(activeData[row][selectedVar])){
          missingFlag = true;
        };
      };

	    //Update the legend with the new data
	    updateLegend();

    };

    /* LEGEND */
	  function updateLegend(){

      // Remove old html
      document.getElementById("legend-title").innerHTML = '';

      // Give legend a title
      document.getElementById("legend-title").innerHTML = variableNameLookup[selectedVar];

      // Remove old legend
      d3_selection.select('#legend').selectAll('svg').remove();

      // Continous legend
      // http://bl.ocks.org/syntagmatic/e8ccca52559796be775553b467593a9f
      
      let min = d3_array.min(activeData, function(d) { return d[selectedVar]; });
      let max = d3_array.max(activeData, function(d) { return d[selectedVar]; });
      let step = (max-min)/3;

      // Find the values to show on the legend
      let legendVals;
      if(percentageVars.includes(selectedVar) || max < 1){
        legendVals = d3_array.range(min,max+.001,step);
        for(let i=0; i < legendVals.length; i++){
          legendVals[i] = d3_format.format(".2")(legendVals[i]);
        }
      } else{
        legendVals = d3_array.range(min,max+1,step);
        for(let i=0; i < legendVals.length; i++){
          legendVals[i] = d3_format.format(",.2r")(legendVals[i]);
        }
      };

      // Set of values to remove duplicates
      let legendValSet = [...new Set(legendVals)];
      let finalVals = [];
      for(let val of legendValSet){
        if(!percentageVars.includes(selectedVar) && max > 1){
          finalVals.push(parseInt(val.replace(',','')));
        } else{
          finalVals.push(val);
        };
      };

      // Color scale
      let legendColorScale = d3_scale.scaleLinear()
                      .domain([min, max])
                      .range(colorGradient)

      // Draw legend 
      var legendHeight = parseInt(getComputedStyle(document.getElementById("legend")).getPropertyValue('height'));
      var legendWidth = parseInt(getComputedStyle(document.getElementById("legend")).getPropertyValue('width'));
      var margin = {top: 10, right: 60, bottom: 10, left: 10};

      var canvas = d3_selection.select("#legend")
        .append("canvas")
        .attr("height", `${legendHeight - margin.top - margin.bottom}px`)
        .attr("width", 1)
        .style("height", `${legendHeight - margin.top - margin.bottom}px`)
        .style("width", `${legendWidth - margin.left - margin.right}px`)
        .style("position", "absolute")
        .style("left", `${margin.left+5}px`)
        .node();
      
      var ctx = canvas.getContext("2d");

      var legendAxisScale = d3_scale.scaleLinear()
        .range([(legendHeight*0.05), legendHeight - margin.top - margin.bottom - (legendHeight*0.05)])
        .domain(legendColorScale.domain())

      d3_array.range(legendHeight).forEach(function(i) {
          ctx.fillStyle = legendColorScale(legendAxisScale.invert(i));
          ctx.fillRect(0, i, 1, 1);
        });

      // Legend axis
      var legendAxis = d3_axis.axisRight()
        .scale(legendAxisScale)
        .tickValues(finalVals);
      
      if(percentageVars.includes(selectedVar)){
        legendAxis.tickFormat(d3_format.format(".0%"));
      } else{
        if(max < 1){
          legendAxis.tickFormat(d3_format.format(",.2f"));
        } else{ 
          legendAxis.tickFormat(d3_format.format(",.0f"));
        }
      } 
    
      var svg = d3_selection.select("#legend")
        .append("svg")
        .attr("height", (legendHeight - margin.top - margin.bottom) + "px")
        .attr("width", (legendWidth) + "px")
    
      svg
        .append("g")
        .attr("class", "axis")
        .attr("transform", "translate(" + (legendWidth - margin.left - margin.right + 10) + "," + (0) + ")")
        .call(legendAxis)
        .call(svg => svg.select(".domain").remove())   
        
      // If any data was mising, add a grey block that shows the color of counties missing data
      if(missingFlag){
        document.getElementById("legend-missing").style.display = "block";
        let missingScale = d3_scale.scaleOrdinal()
                            .domain(['No data'])
                            .range(['#D3D3D3'])

        var missingLegend = d3_selection.select("#legend-missing").append("svg")
          .selectAll("g")
          .data(['No data'])
          .enter()
          .append("g")
          .attr("transform", function(d, i) { return "translate(0," + i * 40 + ")"; });
        
        missingLegend.append("rect")
          .attr('x', 7)
          .attr("width", 30)
          .attr("height", 15)
          .style("position", "absolute")
          .style("fill", missingScale)
          .style("left", `${margin.left}px`);
              
        missingLegend.append("text")
          .data([`‒ No data`])
          .attr("x", 41)
          .attr("y", 6)
          .attr("dy", ".35em")
          .attr("font-family", "Saira")
          .style("font-size", "12px")
          .style("fill", "#777")
          .text(function(d) { return d; });    
      } else{
        document.getElementById("legend-missing").style.display = "none";
      };
	  };

    //VAR and YEAR change listeners
    function updateVarSelect(value) {
      selectedVar = value;
          selectUpdate();
          updateSort();
    };
        
    function updateYearSelect(value) {
  
      selectedYear = value;
  
      dataUrl = newConfig.dataUrl;
            
      getData(dataUrl).then((response) => {
          activeData = response;
          barsAndLabels();
          selectUpdate();
          updateSort();
      }).catch((err) => console.log(err));
  
    };

    //WINDOW RESIZE LISTENER
    if (!eventListenersAdded) { //Only bind the listener once
      document.getElementById('map-bars-var-select').addEventListener('change', function() { updateVarSelect(this.value) });
      document.getElementById('map-bars-year-select').addEventListener('change', function() { updateYearSelect(this.value) }); 
      document.getElementById('map-bars-sort-select').addEventListener('change', function() { updateSort() }); 
     
      d3_selection.select(window).on("resize", function() {
        width = barGraphContainer.node().getBoundingClientRect().width;
        chartAreaDimensions = { width: width - margins.left - margins.right, height: height - margins.top - margins.bottom};
        barsSvg.attr('width', width);
        xScale.range([0, chartAreaDimensions.width]);
        axisTopG.call(xAxis);
        barsAreaG.selectAll('rect').attr('width', function(d) { return xScale(d[selectedVar] )});
        barsAreaG.selectAll('.bar-label').attr('x', function(d) { return (xScale(d[selectedVar]) || 0) + labelPadding });
      });
     
      eventListenersAdded = 1;
    };

    // Look up actual ratio for percentage vars to display in tooltip on bar graph
    function getBarRatio(d){
      let numerator, denominator, numeratorText, denominatorText;
      if(selectedVar == 'per_def_unrep'){
        numerator = d['def_unrep'];
        denominator = d['num_cases'];
        numeratorText = 'unrepresented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_plaint_unrep'){
        numerator = d['plaint_unrep'];
        denominator = d['num_cases'];
        numeratorText = 'unrepresented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_neither_rep'){
        numerator = d['neither_rep'];
        denominator = d['num_cases'];
        numeratorText = 'neither represented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_p100'){
        numerator = d['p100'];
        denominator = d['population'];
        numeratorText = 'in poverty';
        denominatorText = 'people';
      };
      if(selectedVar == 'per_p125'){
        numerator = d['p125'];
        denominator = d['population'];
        numeratorText = 'in poverty';
        denominatorText = 'people';
      };
      if(selectedVar == 'per_accepted'){
        numerator = d['accepted'];
        denominator = d['intakes'];
        numeratorText = 'accepted';
        denominatorText = 'intakes';
      };
      if(selectedVar == 'per_accepted100'){
        numerator = d['accepted'];
        denominator = d['p100'];
        numeratorText = 'accepted';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_accepted125'){
        numerator = d['accepted'];
        denominator = d['p125'];
        numeratorText = 'accepted';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_attorney100'){
        numerator = d['attorneys'];
        denominator = d['p100'];
        numeratorText = 'attorneys';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_attorney125'){
        numerator = d['attorneys'];
        denominator = d['p125'];
        numeratorText = 'attorneys';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'num_cases_per_capita'){
        numerator = d['num_cases'];
        denominator = d['p125'];
        numeratorText = 'cases';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'def_unrep_per_capita'){
        numerator = d['def_unrep'];
        denominator = d['p125'];
        numeratorText = 'cases with unrepresented defendants';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'plaint_unrep_per_capita'){
        numerator = d['plaint_unrep'];
        denominator = d['p125'];
        numeratorText = 'cases with unrepresented plaintiffs';
        denominatorText = 'people in poverty';
      };
      let ratioText = `<div class="tooltip-ratio">${d3_format.format(",")(numerator)} ${numeratorText} / ${d3_format.format(",")(denominator)} ${denominatorText}</div>`;
      return ratioText;
    };

    // Look up actual ratio for percentage vars to display in tooltip on map
    function getMapRatio(county){
      let numerator, denominator, numeratorText, denominatorText;
      if(selectedVar == 'per_def_unrep'){
        numerator = defUnRepLookup[county];
        denominator = numCasesLookup[county];
        numeratorText = 'unrepresented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_plaint_unrep'){
        numerator = plaintUnRepLookup[county];
        denominator = numCasesLookup[county];
        numeratorText = 'unrepresented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_neither_rep'){
        numerator = neitherRepLookup[county];
        denominator = numCasesLookup[county];
        numeratorText = 'neither represented';
        denominatorText = 'cases';
      };
      if(selectedVar == 'per_p100'){
        numerator = poverty100[county];
        denominator = totalPop[county];
        numeratorText = 'in poverty';
        denominatorText = 'people';
      };
      if(selectedVar == 'per_p125'){
        numerator = poverty125[county];
        denominator = totalPop[county];
        numeratorText = 'in poverty';
        denominatorText = 'people';
      };
      if(selectedVar == 'per_accepted'){
        numerator = acceptedLookup[county];
        denominator = intakeLookup[county];
        numeratorText = 'accepted';
        denominatorText = 'intakes';
      };
      if(selectedVar == 'per_accepted100'){
        numerator = acceptedLookup[county];
        denominator = poverty100[county];
        numeratorText = 'accepted';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_accepted125'){
        numerator = acceptedLookup[county];
        denominator = poverty125[county]; 
        numeratorText = 'accepted';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_attorney100'){
        numerator = attorneyLookup[county];
        denominator = poverty100[county];
        numeratorText = 'attorneys';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'per_attorney125'){
        numerator = attorneyLookup[county];
        denominator = poverty125[county];
        numeratorText = 'attorneys';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'num_cases_per_capita'){
        numerator = numCasesLookup[county];
        denominator = poverty125[county];
        numeratorText = 'cases';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'def_unrep_per_capita'){
        numerator = defUnRepLookup[county];
        denominator = poverty125[county];
        numeratorText = 'cases with unrepresented defendants';
        denominatorText = 'people in poverty';
      };
      if(selectedVar == 'plaint_unrep_per_capita'){
        numerator = plaintUnRepLookup[county];
        denominator = poverty125[county];
        numeratorText = 'cases with unrepresented plaintiffs';
        denominatorText = 'people in poverty';
      };
      let ratioText = `<div class="tooltip-ratio">${d3_format.format(",")(numerator)} ${numeratorText} / ${d3_format.format(",")(denominator)} ${denominatorText}</div>`;
      return ratioText;
    };

    /*------------ POPUP -----------*/

    // For map features under the mouse:
    // https://docs.mapbox.com/mapbox-gl-js/example/queryrenderedfeatures/
    // https://ovrdc.github.io/gis-tutorials/mapbox/03-query-features/#4/39.94/-95.52

    //MAP VERSION
    let popupCounty, popupValue, popupContext;
    map.on('mousemove', function(e) {
      if (map.loaded()) { 
        let geoid = String(identifyFeatures(map, e, 'counties', ["GEOID"]).GEOID);

        if (geoid !== 'undefined') {
          popupCounty = fipsToCounty[geoid] ? fipsToCounty[geoid] : 'Not available';
          popupValue = popupValueLookup[geoid];

          if(isNaN(popupValue)){
            popupValue = "No data"
          } else {
              if(percentageVars.includes(selectedVar)){
                popupValue = d3_format.format("0.1%")(popupValue);
                popupContext = getMapRatio(popupCounty);
              } else if(decimalVars.includes(selectedVar)){
                popupValue = d3_format.format(",.1f")(popupValue);
                popupContext = getMapRatio(popupCounty);
              } else{
                popupValue = d3_format.format(",")(popupValue);
                popupContext = ''
              };
          };
          showPopup(mapPopup, e.point.x, e.point.y);
          setPopupContents(mapPopup, popupCounty, popupValue, popupContext);

        } else {
          hidePopup(mapPopup);
        }
      }
    });

    // Change the cursor to a pointer when the it enters a feature in the 'symbols' layer.
    map.on('mouseenter', 'counties', function () {
    	map.getCanvas().style.cursor = 'pointer';
    });
        
    // Change it back to a pointer when it leaves.
    map.on('mouseleave', 'counties', function () {
    	map.getCanvas().style.cursor = '';
    });

    // Format bar chart tool tip
    function barPopUpValue(d){
      if(percentageVars.includes(selectedVar)){
        popupContext = getBarRatio(d);
        return `${d3_format.format(".1%")(d[selectedVar])}`;
      } else if(decimalVars.includes(selectedVar)){
        popupContext = getBarRatio(d);
        return `${d3_format.format(",.1f")(d[selectedVar])}`;
      } else{
        popupContext = '';
        return d3_format.format(",")(d[selectedVar]);
      };
    };

    //BARS VERSION
    barsAreaG.selectAll('rect').on("mouseover", function(event,d) {
      showPopup(popup, event.pageX, event.pageY);
      setPopupContents(popup, d.county, barPopUpValue(d), popupContext);
    })
    .on("mousemove", function(event, d) {
      showPopup(popup, event.pageX, event.pageY);
      setPopupContents(popup, d.county, barPopUpValue(d), popupContext);
    })
    .on("mouseout", function(d) {
      hidePopup(popup);
    });

  }).catch((error) => { console.log(error); });

};