export const dataConfig = {
	'nav-item-representation-eviction': { //Nav menu ID
		title: 'Legal representation across South Carolina counties', //Smaller header above the topic title
		title2: 'Eviction', //Page title (the topic)
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=rep_eviction",
		variables: [//Variable's text is to display in the select menu
			{ text: 'Number of cases', value: 'num_cases' }, //Variable's value should be present in the dataset!
		],
		calculateVariables : [
			{ text: 'Number of cases with unrepresented defendants', value: 'def_unrep' },
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs', value: 'plaint_unrep'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'},
		],
		displayVariables : [
			{ text: 'Number of cases per 1,000 below 125% poverty', value: 'num_cases_per_capita'},
			{ text: 'Number of cases with unrepresented defendants per 1,000 below 125% poverty', value: 'def_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty', value: 'plaint_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'}
		]
	},
	'nav-item-representation-debt': {
		title: 'Legal representation across South Carolina counties',
		title2: 'Debt Collection',
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=rep_debt",
		variables: [//Variable's text is to display in the select menu
			{ text: 'Number of cases', value: 'num_cases' }, //Variable's value should be present in the dataset!
		],
		calculateVariables : [
			{ text: 'Number of cases with unrepresented defendants', value: 'def_unrep' },
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs', value: 'plaint_unrep'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'},
		],
		displayVariables : [
			{ text: 'Number of cases per 1,000 below 125% poverty', value: 'num_cases_per_capita'},
			{ text: 'Number of cases with unrepresented defendants per 1,000 below 125% poverty', value: 'def_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty', value: 'plaint_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'}
		]
	},
	'nav-item-representation-foreclosure': {
		title: 'Legal representation across South Carolina counties',
		title2: 'Foreclosure',
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=rep_foreclosure",
		variables: [//Variable's text is to display in the select menu
			{ text: 'Number of cases', value: 'num_cases' }, //Variable's value should be present in the dataset!
		],
		calculateVariables : [
			{ text: 'Number of cases with unrepresented defendants', value: 'def_unrep' },
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs', value: 'plaint_unrep'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'},
		],
		displayVariables : [
			{ text: 'Number of cases per 1,000 below 125% poverty', value: 'num_cases_per_capita'},
			{ text: 'Number of cases with unrepresented defendants per 1,000 below 125% poverty', value: 'def_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty', value: 'plaint_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'}
		]
	},
	'nav-item-representation-circuit-c&d': {
		title: 'Legal representation across South Carolina counties',
		title2: 'Claim & Delivery (Circuit Court)',
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=rep_circuit_cd",
		variables: [//Variable's text is to display in the select menu
			{ text: 'Number of cases', value: 'num_cases' }, //Variable's value should be present in the dataset!
		],
		calculateVariables : [
			{ text: 'Number of cases with unrepresented defendants', value: 'def_unrep' },
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs', value: 'plaint_unrep'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'},
		],
		displayVariables : [
			{ text: 'Number of cases per 1,000 below 125% poverty', value: 'num_cases_per_capita'},
			{ text: 'Number of cases with unrepresented defendants per 1,000 below 125% poverty', value: 'def_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty', value: 'plaint_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'}
		]
	},
	'nav-item-representation-magistrate-c&d': {
		title: 'Legal representation across South Carolina counties',
		title2: 'Claim & Delivery (Magistrate Court)',
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=rep_magistrate_cd",
		variables: [//Variable's text is to display in the select menu
			{ text: 'Number of cases', value: 'num_cases' }, //Variable's value should be present in the dataset!
		],
		calculateVariables : [
			{ text: 'Number of cases with unrepresented defendants', value: 'def_unrep' },
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs', value: 'plaint_unrep'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'},
		],
		displayVariables : [
			{ text: 'Number of cases per 1,000 below 125% poverty', value: 'num_cases_per_capita'},
			{ text: 'Number of cases with unrepresented defendants per 1,000 below 125% poverty', value: 'def_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented defendants', value: 'per_def_unrep'},
			{ text: 'Number of cases with unrepresented plaintiffs per 1,000 below 125% poverty', value: 'plaint_unrep_per_capita'},
			{ text: 'Percent of cases with unrepresented plaintiffs', value: 'per_plaint_unrep'}
		]
	},
	'nav-item-need-poverty': { 
		title: 'Need across South Carolina counties', 
		title2: 'Poverty', 
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=need_poverty",
		variables: [
			{ text: 'Number of people below 100% poverty', value: 'p100' },
			{ text: 'Number of people below 125% poverty', value: 'p125'},
		],
		calculateVariables : [
			{ text: 'Percent of people below 100% poverty', value: 'per_p100'},
			{ text: 'Percent of people below 125% poverty', value: 'per_p125'}
		],
		displayVariables : [
			{ text: 'Number of people below 100% poverty', value: 'p100' },
			{ text: 'Number of people below 125% poverty', value: 'p125'},
			{ text: 'Percent of people below 100% poverty', value: 'per_p100'},
			{ text: 'Percent of people below 125% poverty', value: 'per_p125'}
		]
	},
	'nav-item-access-intakes': { 
		title: 'Access across South Carolina counties',
		title2: 'Intakes',
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=access_intakes",
		variables: [
			{ text: 'Total intakes', value: 'intakes' },
			{ text: 'Accepted intakes', value: 'accepted'},
		],
		calculateVariables : [
			{ text: 'Percent of intakes accepted', value: 'per_accepted'},
			{ text: 'Accepted intakes per 1,000 below 100% poverty', value: 'per_accepted100'},
			{ text: 'Accepted intakes per 1,000 below 125% poverty', value: 'per_accepted125'}
		],
		displayVariables : [
			{ text: 'Total intakes', value: 'intakes' },
			{ text: 'Accepted intakes', value: 'accepted'},
			{ text: 'Percent of intakes accepted', value: 'per_accepted'},
			{ text: 'Accepted intakes per 1,000 below 100% poverty', value: 'per_accepted100'},
			{ text: 'Accepted intakes per 1,000 below 125% poverty', value: 'per_accepted125'}
		]
	},
	'nav-item-access-attorneys': { 
		title: 'Access across South Carolina counties', 
		title2: 'Attorneys', 
		dataUrl: "https://8gx5njqphd.execute-api.us-east-1.amazonaws.com/?sheetPath=https://docs.google.com/spreadsheets/d/11tEs6AadPYSen-WPpXouG6PUGPAVrjaOHFqY1AW1cmM/gviz/tq?tqx=out:csv&sheet=access_attorneys",
		variables: [
			{ text: 'Total attorneys', value: 'attorneys' }
		],
		calculateVariables : [
			{ text: 'Private attorneys per 1,000 people below 100% poverty', value: 'per_attorney100'},
			{ text: 'Private attorneys per 1,000 people below 125% poverty', value: 'per_attorney125'},
		],
		displayVariables : [
			{ text: 'Total attorneys', value: 'attorneys' },
			{ text: 'Private attorneys per 1,000 people below 100% poverty', value: 'per_attorney100'},
			{ text: 'Private attorneys per 1,000 people below 125% poverty', value: 'per_attorney125'},
		]
	}
}