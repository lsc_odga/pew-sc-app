
import { arrayOfArrayToJson } from '../utils/arrayOfArrayToJson.js';

/**
 * 
 * @param {number} year : ACS year
 * @param {number} oneYearOrFive : 1 or 5 for 1-year or 5-year ACS sample
 * @param {boolean} subjectTable : whether the table is an ACS subject table like S1701 rather than a detailed table
 * @param {array of strings} varsList : array/list of variables, such as ['NAME', 'B02015_009E', 'B02015_009M']. See https://www.census.gov/data/developers/data-sets/acs-1year.html
 * @param {string} forGeoScope : geography type such as "county" or "state" for use in the &for= API call.
 * @param {array of strings} forGeo : array/list of states, counties, etc. for which you want data, such as ["081", "073"]. This will be combined with the forGeoScope into something like "&for=county:081,073". Use strings to ensure leading zeros. Use ["*"] for all of the geography (this should be combined with the "inGeo" parameter below). 
 * @param {string} inGeoScope : geography type such as "county" or "state" for use in the &in= API call.
 * @param {array of strings} inGeo :  array/list of states, counties, etc. WITHIN which you want data. This acts as a filter. For example, ["01", "02"]. This will be combined with the inGeoScope into something like "&for=state:01,02". Use strings to ensure leading zeros.
 * 
 * See geography query format at https://www.census.gov/data/developers/guidance/api-user-guide.Query_Components.html.
 * 
 */
// function buildApiStringForACS(year, oneYearOrFive, subjectTable = true, varsList = [], forGeoType, forGeos = ['*'], inGeoType, inGeos = []) {
// 	const CENSUS_KEY = '0e3e5795cd0335f3faefb832e3f7c9b086d25998';

// 	let subject = subjectTable === true ? '/subject' : '';
// 	let vars = varsList.join(',');
// 	let forG = forGeos.length > 0 ? `&for=${forGeoType}:${forGeos.join(',')}` : '';
// 	let inG = inGeos.length > 0 ? `&in=${inGeoType}:${inGeos.join(',')}` : '';

// 	let apiString = `https://api.census.gov/data/${String(year)}/acs/acs${String(oneYearOrFive)}${subject}?&key=${CENSUS_KEY}&get=${vars}${forG}${inG}`;

// 	console.log(apiString);
// 	return apiString;
// }

export function getCensusData(year, oneYearOrFive, subjectTable = true, varsList = [], forGeoType, forGeos = ['*'], inGeoType, inGeos = []) {

	let fetchedCensusData;

	const CENSUS_KEY = '0e3e5795cd0335f3faefb832e3f7c9b086d25998';

	let subject = subjectTable === true ? '/subject' : '';
	let vars = varsList.join(',');
	let forG = forGeos.length > 0 ? `&for=${forGeoType}:${forGeos.join(',')}` : '';
	let inG = inGeos.length > 0 ? `&in=${inGeoType}:${inGeos.join(',')}` : '';

	let apiString = `https://api.census.gov/data/${String(year)}/acs/acs${String(oneYearOrFive)}${subject}?&key=${CENSUS_KEY}&get=${vars}${forG}${inG}`;

	// console.log(apiString);

	return fetch(apiString)
	.then(function(response) { 
		return response.json(); 
	})
	.then(function(response) {
		//Convert Census API response to proper JSON
		let data = arrayOfArrayToJson(response);

		//For requested variables, replace strings with numeric 
		data.forEach(function(d) {
			for (let i = 0; i < varsList.length; i++) {
				d[varsList[i]] = isNaN(+d[varsList[i]]) ? d[varsList[i]] : +d[varsList[i]];
			}
		});

		return data;
	})
	.catch(function(error) { 
		console.log(error); 
	});

}

