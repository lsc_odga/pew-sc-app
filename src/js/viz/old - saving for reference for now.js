import { drawMapboxMap } from './loadMapboxMap.js';
import { getCensusData } from '../js/censusData.js';
import { mapboxAddLayers } from './mapboxAddLayers.js';
const d3_scale = require('d3-scale');
const d3_array = require('d3-array');

export function setup() {

	// IMPORT INITIAL DATA. THIS IS THE CENSUS POPULATION DATA FOR PER-CAPITA ADJUSTMENTS + ANY DATA TO BE DISPLAYED IN CHARTS APART FROM THE MAP + THE DEFAULT VARIABLE FOR THE MAP (IF ANY)
	let povByCountyFetch = getCensusData(2018, 5, true, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E', 'S1701_C03_001E'], 'county', ['*'], 'state', ['45']);

	// let povByTractFetch = getCensusData(2018, 5, true, ['NAME', 'S1701_C01_001E', 'S1701_C02_001E'], 'tract', ['*'], 'state', ['45']);

	// let scDataFetch = fetch("https://lt2v6wga3l.execute-api.us-east-1.amazonaws.com/default/lsc-data-api?key=spatial/az_basic_field_topojson.json")
	// .then(function(response) {
	// 	return response.json();
	// }).then(function(r) {
	// 	return r.data;
	// })
	


	let map = drawMapboxMap([[-83.353, 32.027],[-78.542, 35.223]], null, null); //SC center = [-80.9, 33.6]

	map.on('load', function(e) {

		// Add tileset sources from studio.mapbox.com. Use the Tileset ID as the URL.
		map.addSource('counties-mapbox-data-source', {
			type: 'vector',
			url: 'mapbox://lsc-odga.8iw4djx1'
		});

		// map.addSource('tracts-mapbox-data-source', {
		// 	type: 'vector',
		// 	url: 'mapbox://lsc-odga.bkew0e65'
		// });
		
		map.addLayer({
			'id': 'counties',
			'type': 'fill',
			'source': 'counties-mapbox-data-source',
			'source-layer': 'Counties_2015-d1pft4', //see https://stackoverflow.com/questions/49755434/mapbox-add-layer-with-vector-source
			'filter': ['==', 'STATEFP', '45'],
			'paint': {
				// 'fill-color': 'hsla(200, 78%, 45%, 0.57)',
				'fill-color': 'rgba(0, 0, 0, 0)',
				'fill-outline-color': 'rgba(50, 115, 220, 1)'
				// 'fill-opacity': 0.6
			},
			'maxzoom': 8,
			'minzoom': 5
		});

		// //County boundaries for tracts
		// map.addLayer({
		// 	'id': 'county-boundaries',
		// 	'type': 'line',
		// 	'source': 'counties-mapbox-data-source',
		// 	'source-layer': 'Counties_2015-d1pft4',
		// 	'filter': ['==', 'STATEFP', '45'],
		// 	'paint': {
		// 		'line-width': 3,
		// 	},
		// 	'maxzoom': 22,
		// 	'minzoom': 8
		// });

		// //Tracts
		// map.addLayer({
		// 	'id': 'tracts',
		// 	'type': 'fill',
		// 	'source': 'tracts-mapbox-data-source',
		// 	'source-layer': 'Census_tracts_2015-8150a0',
		// 	'filter': ['==', 'STATEFP', '45'],
		// 	'paint': {
		// 		'fill-color': 'hsla(200, 78%, 45%, 0.57)',
		// 		'fill-opacity': 0.6
		// 	},
		// 	'maxzoom': 22,
		// 	'minzoom': 8
		// });



		//WAIT FOR THE DATA AND THEN...
		// Promise.all([povByCountyFetch])
		// 	.then(function(dataFiles) {
					
		// 		let povCountyData = dataFiles[0];
		// 		console.log(povCountyData);

		// 		//Linear colors
		// 		let colorLinear = d3_scale.scaleLinear()
		// 			.domain([
		// 				d3_array.min(povCountyData, function(d) { return d.S1701_C03_001E; }),
		// 				d3_array.max(povCountyData, function(d) { return d.S1701_C03_001E; })
		// 			])
		// 			.range(['red', 'blue']);

		// 			console.log(d3_array.min(povCountyData, function(d) { return d.S1701_C03_001E; }),
		// 			d3_array.max(povCountyData, function(d) { return d.S1701_C03_001E; }));


		// 		// GL match expression to assign color to the matching county ID (the indicated spatial data variable)
		// 		let matchExpression = ['match', ['get', 'COUNTYFP']];

		// 		//Pull the right value out the D3 color scale for each geo feature. Build up the match expression.
		// 		povCountyData.forEach(function (row) {
		// 			let color = colorLinear(row['S1701_C03_001E']);
		// 			console.log(row['S1701_C03_001E'], color);
		// 			matchExpression.push(row['county'], color);
		// 		});

		// 		//Last value is default, used where there is no data.
		// 		matchExpression.push('rgba(0, 0, 0, 0)');

		// 		//Set the paint color for each feature
		// 		map.setPaintProperty(
		// 			'counties',
		// 			'fill-color',
		// 			matchExpression
		// 		);
		
		// });
		

	});

}

