export function identifyFeatures(map, location, layer, fields) {
	var queryFeatures = map.queryRenderedFeatures(location.point, { layers: [layer] });
	let returnFeatures = {}

	if (queryFeatures != '') {
    for (let i = 0; i < fields.length; i++) {
			returnFeatures[fields[i]] = queryFeatures[0].properties[fields[i]]
    };
	}
	
	return returnFeatures;
}


export function setChroplethColor(map, data, selectedVar, colorScale) {

	// GL match expression to assign color to the matching county ID (the indicated spatial data variable)
	let matchExpression = ['match', ['get', 'GEOID']];

	//Pull the right value out the D3 color scale for each geo feature. Build up the match expression.
	data.forEach(function (d) {
		let color;
		d.fips = String(d.fips);
		if(isNaN(d[selectedVar])){
			color = "#D3D3D3";
		} else{
			color = colorScale(d[selectedVar]);
		}
		matchExpression.push(d['fips'], color);
	});

	//Last value is default, used where there is no data.
	matchExpression.push('rgba(0, 0, 0, 0)');

	//Set the paint color for each feature
	map.setPaintProperty(
		'counties',
		'fill-color',
		matchExpression
	);

}