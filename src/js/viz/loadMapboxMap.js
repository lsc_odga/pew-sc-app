import "mapbox-gl/dist/mapbox-gl.css"
import mapboxgl from 'mapbox-gl';



//https://docs.mapbox.com/mapbox-gl-js/api/
//https://docs.mapbox.com/mapbox-gl-js/example/data-join/
//https://uscensusbureau.github.io/citysdk/examples/mapbox-choropleth/
//https://tigerweb.geo.census.gov/tigerwebmain/TIGERweb_restmapservice.html
//https://tigerweb.geo.census.gov/arcgis/rest/services/Basemaps/CommunityTIGER/MapServer
//https://github.com/mapbox/awesome-vector-tiles
//https://stackoverflow.com/questions/63713703/mapbox-gl-js-coloring-individual-features-in-large-geojson
//https://docs.mapbox.com/help/glossary/data-driven-styling/
//https://docs.mapbox.com/mapbox-gl-js/style-spec/expressions/#!=
//https://docs.mapbox.com/mapbox-gl-js/example/check-for-support/
//https://docs.mapbox.com/help/troubleshooting/working-with-large-geojson-data/
//https://docs.mapbox.com/mapbox-gl-js/example/queryrenderedfeatures/
//https://github.com/mapbox/real-time-maps

//Mapbox has the boundaries but must pay for them. Cost = unknown. https://docs.mapbox.com/vector-tiles/reference/mapbox-boundaries-v3/









export let mapHasBeenDrawn = false, countyBoundariesHaveBeenDrawn = false;


/**
 * Using fitBounds is ideal and in that case the center and zoom are not necessary.
 * @param {array} center : starting position as [lng, lat]
 * @param {number} zoom : starting zoom level
 * @param {array of arrays} fitBounds : bounding box. See https://docs.mapbox.com/mapbox-gl-js/api/map/#map#fitbounds
 */

// export function drawMapboxMap(center = null, zoom = null, fitBounds = null) {
export function drawMapboxMap(center = null, zoom = null) {

	mapboxgl.accessToken = 'pk.eyJ1IjoibHNjLW9kZ2EiLCJhIjoiY2p3YXNjd2E5MDhrbDQ5bjQxZGRscXdycCJ9.OqlR963BX2EZfGyd7u2GqA';

  let p = {padding: {top: 20, bottom: 20, left: 20, right: 20}};
	const map = new mapboxgl.Map({
		attributionControl: false,
		container: 'map-container',
		style: 'mapbox://styles/mapbox/light-v10', // stylesheet location
		// style: 'mapbox://styles/lsc-odga/ck0pedgmr06bi1co1sx039sez',
		maxZoom: 13,
    // center: center,
    // zoom: zoom
	});

  map.fitBounds([
//     [32.958984, -5.353521],
// [43.50585, 5.615985]
    [-83.35391, 32.0346],
    [-78.54203, 35.215402]
  ],{padding: p});

	// if (center) { map.center = center; }
	// if (zoom) { map.zoom = zoom; }
	// if (fitBounds) { 
	// 	map.fitBounds(
	// 		fitBounds,
	// 		{padding: {top: 20, bottom: 20, left: 20, right: 20}}
	// 	);
	// }

	map.addControl(new mapboxgl.AttributionControl({
		compact: true
	}), "top-right");
	map.scrollZoom.disable();
	map.addControl(new mapboxgl.NavigationControl({"showCompass": false}), "bottom-right");

	mapHasBeenDrawn = true;

	return map;
}



export function addCountyBoundaries(map) {
	map.addSource('counties-mapbox-data-source', {
		type: 'vector',
		url: 'mapbox://lsc-odga.8iw4djx1'
	});
	
	map.addLayer({
		'id': 'counties',
		'type': 'fill',
		'source': 'counties-mapbox-data-source',
		'source-layer': 'Counties_2015-d1pft4', //see https://stackoverflow.com/questions/49755434/mapbox-add-layer-with-vector-source
		'filter': ['==', 'STATEFP', '45'],
		'paint': {
			// 'fill-color': 'hsla(200, 78%, 45%, 0.57)',
			'fill-color': 'rgba(0, 0, 0, 0)',
			'fill-outline-color':  'rgba(148,156,157,1)'//'rgba(50, 115, 220, 1)'
			// 'fill-opacity': 0.6
		},
		'maxzoom': 13,
		'minzoom': 5
	});
  
	countyBoundariesHaveBeenDrawn = true;
}

export const scCoords = [[-83.353, 32.027],[-78.542, 35.223]];