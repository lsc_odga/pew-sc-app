const path = require("path");
const common = require("./webpack.common");
const { merge } = require("webpack-merge");
var HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(common, {
	mode: "development",
  output: {
    filename: "[name].bundle.js",
		path: path.resolve(__dirname, "dist")
  },
  plugins: [
    new HtmlWebpackPlugin({
			template: "./src/index.html",
		})
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader"
				]
      },
      {
        test: /\.csv$/,
        loader: 'csv-loader',
        options: {
          dynamicTyping: true,
          header: true,
          skipEmptyLines: true
        }
      },
      {
        test: /\.(mp4|svg|png|jpg|gif)$/,
        use: {
          loader: "file-loader",
          options: {
						// name: "[name].[hash].[ext]"
						name(resourcePath, resourceQuery) {
							if (process.env.NODE_ENV === 'development') {
								return '[path][name].[ext]';
							}
	
							return '[contenthash].[ext]';
						},
						outputPath: "assets",
						publicPath: "assets"
          }
        }
      }
    ]
  }
});
