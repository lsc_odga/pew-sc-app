const path = require("path");
const common = require("./webpack.common");
const { merge } = require("webpack-merge");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const autoprefixer = require('autoprefixer');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = merge(common, {
  mode: "production",
  output: {
    // filename: "[name].[contentHash].bundle.js",
    filename: "[name].bundle.js",
		path: path.resolve(__dirname, "dist/"),
		publicPath: "", //e.g., /civil-legal-aid-data/lsc-allocations-process/process/
		// environment: { //what do you expect the browser to support for the resulting webpack code? Assuming we care about IE here.
		// 	arrowFunction: false,
		// 	const: false,
		// 	destructuring: false,
		// 	dynamicImport: false,
		// 	forOf: false,
		// 	module: false
		// },
		chunkFilename: '[chunkhash].js' //to solve module call error. https://github.com/webpack/webpack/issues/5429
  },
  optimization: {
		concatenateModules: false, // ditto
		providedExports: false, //ditto
		usedExports: false, //ditto
		splitChunks: { chunks: "all" },
    minimizer: [
      new OptimizeCssAssetsPlugin()
      // new TerserPlugin()
    //   new HtmlWebpackPlugin({ //Include this if you want to simplify the HTML output
		// 		template: "./src/index.html",
    //     minify: {
    //       removeAttributeQuotes: true,
    //       collapseWhitespace: true,
    //       removeComments: true
    //     }
		// 	})
    ]
  },
  plugins: [
    // new MiniCssExtractPlugin({ filename: "[name].[contentHash].css" }),
    new BundleAnalyzerPlugin(),
    new MiniCssExtractPlugin({ filename: "[name].css" }),
		new HtmlWebpackPlugin({
			template: "./src/index.html",
		}),
    new CleanWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
					"css-loader",
					'postcss-loader',
          "sass-loader"
        ]
      },
      {
        test: /\.csv$/,
        loader: 'csv-loader',
        options: {
          dynamicTyping: true,
          header: true,
          skipEmptyLines: true
        }
      },
      {
				test: /\.js$/,
				exclude: /node_modules/,
        use: {
					loader: "babel-loader"
				}
			},
      {
        test: /\.(mp4|svg|png|jpg|gif)$/,
        use: {
          loader: "file-loader",
          options: {
						// name: "[name].[hash].[ext]"
						name(resourcePath, resourceQuery) {
							// `resourcePath` - `/absolute/path/to/file.js`
							// `resourceQuery` - `?foo=bar`
	
							if (process.env.NODE_ENV === 'development') {
								return '[path][name].[ext]';
							}
	
							return '[contenthash].[ext]';
						},
						outputPath: "assets",
						publicPath: "assets", //e.g., /civil-legal-aid-data/lsc-allocations-process/process/
          }
        }
      }
    ]
  }
});
